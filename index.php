﻿<html>
<?php
	require_once "class/core/Config.class.php"; 
	$cfg = new \core\Config("user");
	$doc = new \core\Document($cfg);
	
	foreach($cfg->def_box AS $box) {
		$cfg->debug1 .= \core\constFix($box[2] )."-".\core\constFix($box[1] )."<br />";
		if( ($ipt = \core\inputFix(\core\constFix($box[2] ), \core\constFix($box[1] ), '') ) !== '' ) {
			$cfg->debug1 .= "-->box:".$box[3];
			$params = [];
			$params[] = $ipt;
			foreach($box[4] AS $prm => $alt) {
				$params[] = \core\inputFix($box[2], \core\constFix($box[1] ).$prm, $alt);
			}
			$doc->container->addBox(new $box[3]($cfg, $params[0] ) );
		}
	}
	echo $doc->draw();
	
?>	
</html>