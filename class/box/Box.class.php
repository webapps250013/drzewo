<?php
namespace box;

class Box {
	public $boxHeader;
	public $boxContent;
	public $boxButtons;
	public $boxLinks;
	
	protected $color_prefix = UX_COLOR_MAIN;
	protected $mainObj;
	protected $headerImg;

    protected $cfg;      // [? make singleton in Config class]

	public function __construct($color_prefix = UX_COLOR_MAIN) {
		$this->color_prefix = $color_prefix;
	}

	public function draw() {
		$out = "<div class='boxContainer shadow ".$this->color_prefix."Color' id='".get_class($this).$this->mainObj->getId()."'>";
		$out .= $this->boxHeader->draw();
		$out .= $this->boxContent->draw();
		$out .= $this->boxButtons->draw();
		$out .= "</div>";
		$out .= $this->boxLinks->draw();
		$out .=
			"<script>
				showBoxContentList(".get_class($this).$this->mainObj->getId().".getElementsByClassName('boxContent')[0] , 'boxContentDesc');
			</script>";
		
		return $out;
	}
	
	protected function init($cfg, $obj, $objType, $objClass, $colorSpecific = false) {
		$this->cfg = $cfg;
		$objClass = '\db\\'.$objClass;
		if(is_object($obj) && $obj->getObjType() != $objType) {
			$obj = new $objClass($cfg, 0);
		}
		else if(!is_object($obj) ) {
			$obj = new $objClass($cfg, $obj);
		}
		$this->mainObj = $obj;
		
		$this->color_prefix = $this->mainObj->getColorPrefix($colorSpecific);
		$this->headerImg = $this->mainObj->getImg("BOX")->draw();
		
		$this->boxHeader = new BoxHeader();
		$this->boxContent = new BoxContent($this->color_prefix);
		$this->boxButtons = new BoxButtons();
		$this->boxLinks = new BoxLinks($this->mainObj);
		
		return $this->mainObj;
	}
}
