<?php
namespace box;

class BoxButtons {
	private $lText = GLOB_ANULUJ;
	private $rText = GLOB_OK;
	private $lAnchor = "";
	private $rAnchor = "";
	private $lClick = "";
	private $rClick = "";
	
	public function setLeftButton($text = GLOB_ANULUJ, $anchor = "", $click = "") {
		$this->lText = $text;
		$this->lAnchor = $anchor;
		$this->lClick = $click;
	}
	public function setRightButton($text = GLOB_OK, $anchor = "", $click = "") {
		$this->rText = $text;
		$this->rAnchor = $anchor;
		$this->rClick = $click;
	}
	
	public function draw() {
		return 
			"<div class='boxButtons'>"
				."<div class='boxBtt' ".($this->lClick != "" ? "onclick='".$this->lClick."'" : "").">"
					.($this->lAnchor != "" ? "<a href='".$this->lAnchor."'>".$this->lText."</a>" : $this->lText)
				."</div>"
				."<div class='boxBttX' onclick='displayType(infoBlok)'>".UX_BTT_CLOSE."</div>"
				."<div class='boxBtt' ".($this->rClick != "" ? "onclick='".$this->rClick."'" : "").">"
					.($this->rAnchor != "" ? "<a href='".$this->rAnchor."'>".$this->rText."</a>" : $this->rText)
				."</div>"
			."</div>";
	}
}
