<?php
namespace box;

class boxHeader {
	private $content = "";
	
	private $color_prefix;
	private $objId;
	
	public function __construct() {}
	
	public function addObj($line1, $line2, $line3, $line4, $img, $img_pos = "left") {
		$lines = $this->txtLines($line1, $line2, $line3, $line4);
		$img = $this->imgDiv($img);
		
		switch($img_pos) {
			case "left":	$this->content .= $img.$lines;
							break;
			case "right":	$this->content .= $lines.$img;
							break;			
			case "img":		$this->content .= $img;
							break;
			default:		$this->content .= $lines;
		}
	}
	
	
	public function draw() {
		return "<div class='boxHeader'>"
				.$this->content
			."</div>";
	}
	
	private function txtLines($line1, $line2, $line3, $line4) {
		return "<div class='boxHeaderTxt fontBig'>"
						."<div class='fontBigger'>".$line1."</div>"
						.(strlen($line2) > 0 ? "<div>".$line2."</div>" : "")
						.(strlen($line3) > 0 ? "<div>".$line3."</div>" : "")
						.(strlen($line4) > 0 ? "<div>".$line4."</div>" : "")
				."</div>";
	}
	
	public function imgdiv($img) {
		return $img;
	}
}
