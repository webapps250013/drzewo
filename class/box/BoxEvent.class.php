<?php
namespace box;

final class BoxEvent extends Box {
	private $headerPeople = "";
	private $headerPlaces = "";
	
	public function __construct($cfg, $event) {
		$event = $this->init($cfg, $event, OBJ_EVENT, "DbEvent");
		
		if($event->getId() == '0')
			$this->eventList();
		else
			$this->eventOne($event);
	}
	private function eventOne($event) {
		/*
		nazwa wydarzenia				|___
		osoby1							| e |
		data1 - [data2]					|---|
		miejsce							|
		--------------------------------|
		osoby[A]		| lista			|
		miejsca[A]		|				|
		powiązane wyd.	|				|
		--------------------------------|
		wyd_lewy	|	X	| wyd_prawy |
		--------------------------------|
		*/
		$eventPeople1	= $event->getPeople(1); // tmp
		$eventPeople	= $event->getPeople();
		$eventPlaces	= $event->getPlaces();
		$eventRelated	= $event->getEvents();

		if($eventPeople1->getAmount() > 0 ) {
			$this->headerPeople .= $eventPeople1->draw("", LTYPE_INLINE);
		}
		else {
			$this->headerPeople = "—"; // [? stałe]
		}
		$this->headerPlaces = "<span>".UX_LABEL_PLACES.": "; // [? miejsca z type = 1]
		
		$this->boxHeader->addObj(	$event->getName(),
									$this->headerPeople,
									$event->lifetime->getLength(0, 1),
									$this->headerPlaces,
									$this->headerImg);
				
		$this->boxContent->addList("boxContentListPeople", \core\constFix("UX_LIST_LABEL_PEOPLE"),			$eventPeople,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListPlaces", \core\constFix("UX_LIST_LABEL_PLACES"),			$eventPlaces,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListEvents", \core\constFix("UX_LIST_LABEL_RELATED_EVENTS"),	$eventRelated,	LTYPE_LIST_GENERIC);
		
		$this->boxButtons->setLeftButton(UX_BOX_EVENT_BTT_LEFT);
		$this->boxButtons->setRightButton(UX_BOX_EVENT_BTT_RIGHT);
		
	}
	
	private function eventList() {
		$date = date("Ymd").UX_DATE_FLAG_DAY;
		$day = new \core\Lifetime($date, $date, $date, $date);
		
		$date = date("md");
		$yr = date("Y");
		$type = "%";
		////////////////// drzewo31
		$db = $this->cfg->db;
		$evtsLst = $db->getDbData("getAnniversaryList", [$date, "-99999999".UX_DATE_FLAG_DAY, $yr.$date.UX_DATE_FLAG_DAY, $type], OBJ_EVENT);
		
		$evts = $evtsLst->getItems();
		$evtsLst1 = new \core\ListOfItems($this->cfg, [], LTYPE_LIST_GENERIC,	["class" => "boxContentListSoon"], ["1", "4", "2"] );
		$evtsLst2 = new \core\ListOfItems($this->cfg, [], LTYPE_LIST_GENERIC,	["class" => "boxContentListLast"], ["1", "4", "2"] );
		
		$this->cfg->debug1 .= "<br />evts:".count($evts).";<br/>";
		
		$annivListA = [10];
		$annivListB = [18];
		
		$this->boxHeader->addObj(	\core\constFix("UX_LABEL_EVENTS"),
									"",
									$day->getDateStart(),
									"",
									$this->headerImg);
		
		$this->boxContent->addToListLeft("boxContentListCalendar",	\core\constFix("UX_BOX_EVENTS_LABEL_CAL") );
		
		$this->boxContent->addList("boxContentListToday",	\core\constFix("UX_BOX_EVENTS_LABEL_TODAY"),	$evtsLst,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListLast",	\core\constFix("UX_BOX_EVENTS_LABEL_LAST"),	$evtsLst2,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListSoon",	\core\constFix("UX_BOX_EVENTS_LABEL_SOON"),	$evtsLst1,	LTYPE_LIST_GENERIC);
		
		$this->boxButtons->setLeftButton(UX_BOX_EVENTS_BTT_LEFT);
		$this->boxButtons->setRightButton(UX_BOX_EVENTS_BTT_RIGHT);
	}
}
