<?php
namespace box;

class BoxLinks {
	private $links = [];		// dbLink objs
	
	private $mainObj;
	
	public function __construct($obj) {
		$this->mainObj = $obj;
	}
	
	public function addLinks($lnk) {
		$this->links = $lnk;
	}
	
	public function draw() {
		$out = "<ul class='boxLinks shadow'>
					<li class='boxLinksItem' onClick='fetchBox(this.parentElement.parentElement, &quot;".$this->mainObj->getObjType()."&quot;, ".$this->mainObj->getId().".)' >
						<a href='?e=1&ei=".$this->mainObj->getId()."'>".UX_BTT_EDIT."</a>
					</li>";
		$lnk_tmp = new \db\DbLink(null, null);
		for($i = 0; $i < count($this->links); $i++) {
			$out .= "<li class='lnk lnk".$this->links[$i][5]."'>".
					$lnk_tmp->makeLink($this->links[$i][1], 			// prefix
							$this->links[$i][3], 						// sufix
							$this->links[$i][2], 						// subpage
							$this->links[$i][0], 						// name
							$this->links[$i][4], 						// icon
							$this->links[$i][6] )						// method
				."</li>";
		}
		$out .= "</ul>";
		
		return $out;
	}
}
