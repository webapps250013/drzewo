<?php
namespace box;

class BoxContent {
	private $listLeft = [];
	private $listHidden = [];
	private $img;
	private $txt;
	
	private $color_prefix;
	private $objId;
	
	public function __construct($color_prefix = UX_COLOR_MAIN) {
		$this->color_prefix = $color_prefix;
	}
	
	public function addToListLeft($className, $str, $func = "", $fetchParams = "", $fetchVals = "") {
		$this->listLeft[] = [$className, $str, $func, $fetchParams, $fetchVals];
	}
	public function addToListHidden($className, $listObj, $listType) {
		if(is_array($this->listHidden) )
			$this->listHidden = "";
		$this->listHidden .= $listObj->draw($className, $listType);
	}
	public function addList($className, $label, $listObj, $listType, $func = "", $fetchParams = "", $fetchVals = "") {
		$this->listLeft[] = [$className, $label, $func, $fetchParams, $fetchVals];
		if(is_array($this->listHidden) )
			$this->listHidden = "";
		$this->listHidden .= $listObj->draw($className, $listType);
	}
	public function addFetchList($className, $label, $listObj, $listType, $func = "", $fetchParams = "", $fetchVals = "") {
		$this->listLeft[] = [$className, $label, $func, $fetchParams, $fetchVals];
	}
	public function addParagraph($className, $label, $content) {
		$this->listLeft[] = [$className, $label, "", "", ""];
		if(is_array($this->listHidden) )
			$this->listHidden = "";
		$this->listHidden .= '<p class="'.$className.'">'.$content.'</p>';
	}
	
	public function draw() {
		$out = "<div class='boxContent'>
			<div class='boxContentLeft'>
				<ul class='".$this->color_prefix."Color'>";
		foreach($this->listLeft AS $item)
			$out .= '<li onclick="showBoxContentList'.$item[2].'('
						.($item[2] == "Db" ? 
							"this, '".$item[0]."', '".$item[3]."', '".$item[4]."', " :
							"this.parentElement.parentElement.parentElement, '".$item[0]."'")
					.')">'.$item[1].'</li>';
		$out .= "
				</ul>
			</div>
			<div class='boxContentRight'>
				<p>".GLOB_JS_KOMUNIKAT."</p>
			</div>
			<div class='boxContentHidden'>"
				.$this->listHidden
			."</div>
		</div>";
		
		return $out;
	}
}
