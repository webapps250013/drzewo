<?php
namespace box;

final class BoxPlace extends Box {
	
	public function __construct($cfg, $place) {
		$place = $this->init($cfg, $place, OBJ_PLACE, "DbPlace");
		
		$headerNameLine = $place->getName();
		$headerAddressLine = "[addr]";
		$headerTypeLine = $place->getSubtype();
		$headerStatusLine = $place->getStatus();
		$headerImg = $place->image->draw("BOX");
		
		$events	= $place->getEvents();
		$people = $place->getPeople();
		$subplaces = $place->getPlaces();
		
		$this->boxHeader->addObj($headerNameLine, $headerTypeLine, $headerAddressLine, $headerStatusLine, $headerImg);
		
		$this->boxContent->addList("boxContentListPeople", \core\constFix("UX_LIST_LABEL_PEOPLE"),	$people,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListPlaces", \core\constFix("UX_LIST_LABEL_PLACES"),	$subplaces,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListEvents", \core\constFix("UX_LIST_LABEL_EVENTS"),	$events,	LTYPE_LIST_GENERIC);
		
		$this->boxButtons->setLeftButton(UX_BOX_PLACE_BTT_LEFT, "?".OBJ_PLACE."=".$this->mainObj->getId() );
		$this->boxButtons->setRightButton(UX_BOX_PLACE_BTT_RIGHT, "?".OBJ_PLACE."=".$this->mainObj->getId() );
		
		//$this->boxLinks->addLinks($lnk);
	}
}
