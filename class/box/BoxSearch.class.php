<?php
namespace box;

final class BoxSearch extends Box {
	
	public function __construct($cfg, $string) {
		
		$contentListLeft[] = ["boxContentListPeople",	\core\constFix("UX_LIST_LABEL_PEOPLE"),  "", "", ""];
		$contentListLeft[] = ["boxContentListPlaces",	\core\constFix("UX_LIST_LABEL_PLACES"), "", "", ""];
		$contentListLeft[] = ["boxContentListEvents",	\core\constFix("UX_LIST_LABEL_EVENTS"), "", "", ""];
		
		$contentListHidden = "";
		
		$this->boxHeader->addObj(UX_LABEL_SEARCHBOX, "Ciąg znaków: ".$string, "Trafień: "."n", "", null);
	
		$this->boxContent->addListLeft($contentListLeft);
		$this->boxContent->addListHidden($contentListHidden);
		
		$this->boxButtons->setLeftButton(UX_BOX_SEARCH_BTT_LEFT, "?".OBJ_PLACE."=");
		$this->boxButtons->setRightButton(UX_BOX_SEARCH_BTT_RIGHT, "?".OBJ_PLACE."=");
	}
	
}
