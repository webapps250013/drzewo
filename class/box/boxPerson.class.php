<?php
namespace box;


final class BoxPerson extends Box {
	
	public function __construct($cfg, $person) {
		$person = $this->init($cfg, $person, OBJ_PERSON, "DbPerson", true);
		$db = $cfg->db;
		
		$events	= $person->getEvents();
		$places	= $person->getPlaces();
		$names  = $person->getNames();
		$ships  = $person->getShips();
		$lnk	= $person->getLinks();
		
		if($person->lifetime->hasEnd() ) {
			$plLine = $db->getDbData('getPersonEventsByPrimType', [$this->mainObj->getId(), "DBODY_MNTRNCE"], OBJ_EVENT);
		}
		else {
			$plLine = $db->getDbData('getPersonEventsByPrimType', [$this->mainObj->getId(), "RESIDENCE"], OBJ_EVENT);
		}
		if($plLine->getAmount() == 0) {
			$headerPlaceLine = "—";
		}
		else {
			$lstObj = $plLine->getLast()["obj"];
			$headerPlaceLine = $lstObj->extra_txt["placeType"].": "
				."<span id='infoMs' class='placeColorRev miejscFont'>"
					."<a href='index.php?".OBJ_PLACE."=".$lstObj->extra_txt["placeId"]."'>"./*$cfg->db->msUp*/($lstObj->extra_txt["placeId"] )/*[1]*/."</a>"
				."</span>";
		}
		
		$showShipList = 0;
		$headerStatusLine = "";
		if($person->lifetime->isConsentAge() || count($ships) > 0) {
			$stt = "0";
			$sexStr = ($person->sex == GLOB_F ? GLOB_M : GLOB_F);
			foreach($ships AS $ship) {
				if($ship->getStatus() != 0) {
					$shipPartner = $ship->getOtherPerson($person);
					$headerStatusLine .= ($stt == $ship->getSubtype() ? 
											"" : 
											" ".$ship->getName("person".$sexStr).":")
										." "
										."<span class='personColorRev'>
											<a href='?".OBJ_PERSON."=".$shipPartner->getId()."'>".$shipPartner->getName()."</a>"
										."</span>";
					$stt = $ship->getSubtype();
				}
				else {
					$showShipList = 1;
				}
			}
			if($stt == "0") {
				switch($person->shipStatus) {
					case "1":	$headerStatusLine = \core\constFix("SHIPS_NO_DATA_".$person->sex);
								break;
					case "2":	$headerStatusLine = \core\constFix("SHIPS_CELIBACY_".$person->sex);
								break;
					default:	$headerStatusLine = \core\constFix("SHIPS_NO_PARTNER_".$person->sex);
								break;
				}
			}
		}
		else {
			$parent[0] = $person->father;
			$parent[1] = $person->mother;
			$headerStatusLine = \core\constFix("UX_LIST_LABEL_PARENTS").":";
			for($i = 0; $i < 2; $i++)
				$headerStatusLine .= 
					" <span class='personColorRev'>"
						."<a href='?".OBJ_PERSON."=".$parent[$i]->getId()."'>".$parent[$i]->getName()."</a>"
					."</span>";
		}

		$headerNameLine = $person->getName();
		$headerDateLine = $person->lifetime->getLength(0, 1);
		
		$this->boxContent->addParagraph("boxContentDesc", 				\core\constFix("UX_LIST_LABEL_DESC"), $person->desc);
		$this->boxContent->addToListLeft("boxContentListNames", 		\core\constFix("UX_LIST_LABEL_NAMES") );
		$this->boxContent->addList("boxContentListPlaces", 				\core\constFix("UX_LIST_LABEL_PLACES"), $places,	LTYPE_LIST_GENERIC);
		$this->boxContent->addList("boxContentListEvents", 				\core\constFix("UX_LIST_LABEL_EVENTS"), $events,	LTYPE_LIST_GENERIC);
		
		if(count($ships) > 1 || $showShipList)
			$this->boxContent->addToListLeft("boxContentListShips",		\core\constFix("UX_LIST_LABEL_SHIPS"), 	"Db", "L".OBJ_PERSON."M", $this->mainObj->getId() );
		if($person->father != NULL || $person->mother != NULL) {
			$this->boxContent->addToListLeft("boxContentListParents", 	\core\constFix("UX_LIST_LABEL_PARENTS"), 	"Db", "L".OBJ_PERSON."R", $this->mainObj->getId() );
			$this->boxContent->addToListLeft("boxContentListSiblings", 	\core\constFix("UX_LIST_LABEL_SIBLINGS"), "Db", "L".OBJ_PERSON."S", $person->father->getId()."-".$person->mother->getId()."-".$this->mainObj->getId() );
		}
		if($person->childAmount > 0)
			$this->boxContent->addToListLeft("boxContentListChildren", 	\core\constFix("UX_LIST_LABEL_CHILDREN"), "Db", "L".OBJ_PERSON."C", $this->mainObj->getId() );
		
		$pTypes = [];
		$pTypes['fn'] = ['listClass' => 'boxContentListFirstName', 'listLabel' => 'UX_LIST_LABEL_SAMEFNAME', 'itemClass' => 'infoImie'];
		$pTypes['ln'] = ['listClass' => 'boxContentListLastName',  'listLabel' => 'UX_LIST_LABEL_SAMELNAME', 'itemClass' => 'infoNazw'];
		foreach($pTypes as $pType => $pVals) {
			$used = [];
			foreach($names AS $kItems => $vItems) {
				foreach($vItems->parts AS $k => $parts) {
					foreach($parts AS $v) {
						if($v["npartType"] == $pType) {
							$skip = 0;
							foreach($used AS $u)
								if($u == $v["npartId"] )
									$skip = 1;
							if($skip) continue;
							$used[] = $v["npartId"];
							$this->boxContent->addToListLeft($pVals['listClass'].$v['npartId'], \core\constFix($pVals['listLabel'] )." <span class='".$pVals['itemClass']."'>".$v["npartString"], "Db", "L".OBJ_PERSON."V", $v["npartString"] );
						}
					}
				}
			}
		}
		
		if($person->lifetime->isExactDate('1', UX_DATE_FLAG_YEAR) ) {
			$bDate = $person->lifetime->getDateStart(2, 1);
			$this->boxContent->addToListLeft("boxContentListBYear", \core\constFix("UX_LIST_LABEL_SAMEBYEAR")." ".$bDate, "Db", "L".OBJ_PERSON."V", $bDate );
		}
				
		$this->boxHeader->addObj($headerNameLine, $headerDateLine, $headerPlaceLine, $headerStatusLine, $this->headerImg);		
		
		$this->boxButtons->setLeftButton(UX_BOX_PERSON_BTT_LEFT, "?".OBJ_SHIP.OBJ_PERSON."=".$this->mainObj->getId());
		$this->boxButtons->setRightButton(UX_BOX_PERSON_BTT_RIGHT, "?".ATTR_BLOOD."=".$this->mainObj->getId());
		
		$this->boxLinks->addLinks($lnk);
	}
}
