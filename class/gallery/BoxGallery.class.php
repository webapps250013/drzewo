<?php

final class BoxGallery extends \box\Box {
	private $objId;		// [?tmp / to inspect

	public function __construct($cfg, $obj) {
		$this->color_prefix = UX_COLOR_IMAGE;
		$db = $cfg->db;
		
		if(is_object($obj) ) {
			$this->objId = $obj->getId();
		}
		else {
			$this->objId = $obj;
			$obj = new \db\DbPerson($cfg, $this->objId);
		}
		
		switch($obj->getObjType() ) {
			case OBJ_EVENT:	$images = $db->getDbData("getEventPhotos", [$this->objId], OBJ_IMAGE);
							break;
			case OBJ_PERSON:$images = $obj->getImages();
							break;
			default:		exit(ERR_D00_T);
		}

		if(count($images) == 0)
			$images[] = new \db\DbImage($cfg, 0);

		$headerImg = $images[0]->image->draw("BOX-GALLERY");
		$contentListLeft[] = ["boxContentListPeople", \core\constFix("UX_LIST_LABEL_PEOPLE"), "", "", ""];
		$contentListLeft[] = ["boxContentListPlaces", \core\constFix("UX_LIST_LABEL_PLACES"), "", "", ""];
		$contentListLeft[] = ["boxContentListEvents", \core\constFix("UX_LIST_LABEL_RELATED_EVENTS"), "", "", ""];
		
		$this->prepare();
		
		$this->boxHeader->addObj("", "", "", "", $headerImg, "img");
		
		$this->boxContent->addParagraph("Paragraph");
		
		$this->boxButtons->setLeftButton(UX_BOX_IMAGES_BTT_LEFT);
		$this->boxButtons->setRightButton(UX_BOX_IMAGES_BTT_RIGHT);
		
	}
}