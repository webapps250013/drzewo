<?php
namespace db;

final class DbName extends DbObj {
	public $personId;
	public $nameString = "";
	public $data = [];
	public $parts = [];
	
	private $nParts = [];
	
	public function __construct($cfg, $nameId) {
		$db = $cfg->db;
		$this->objId = $nameId;
		$this->obj_type = OBJ_NAME;
		
		$this->color_prefix_general = UX_COLOR_MAIN;
		$this->color_prefix_specific = UX_COLOR_MAIN;
		
	}
    /*
	private function test() {
		if(count($tzs = $db->getDbDataArray("getIdentities", [$id] ) ) == 0) {
			$this->mainName = "";
			$this->data = [];
			$this->parts = [];
			return;
		}
		$last = 0;
		foreach($tzs AS $v) {
			if($last == $v[0] )
				continue;
			$last = $v[0];
			$this->mainName = $v[10];
			$this->data[]["nameId"] = $v[0];
			$this->data[count($this->data) - 1]["nameString"] = $v[11];
			$this->data[count($this->data) - 1]["eventId"] = $v[7];
			$this->data[count($this->data) - 1]["eventType"] = $v[8];
			$this->data[count($this->data) - 1]["rcnstr"] = $v[9];
		}
		foreach($tzs AS $v) {
			$this->parts[$v[0] ][]["npartId"] = $v[3];
			$this->parts[$v[0] ][count($this->parts[$v[0] ] ) - 1]["npartString"] = $v[2];
			$this->parts[$v[0] ][count($this->parts[$v[0] ] ) - 1]["prefix"] = $v[1];
			$this->parts[$v[0] ][count($this->parts[$v[0] ] ) - 1]["visible"] = $v[6];
			$this->parts[$v[0] ][count($this->parts[$v[0] ] ) - 1]["npartType"] = $v[4];
		}
	}*/

	public function getNameString($textOnly = 0) {
		return $this->personNameFix($this->nameString, $textOnly);
	}
	
	private function personNameFix($str, $textOnly = 0) { // [? ]
		/********/
		// +NAZWISKO+
		// *IMIE*
		// #ALIAS#
		/********/
			
		$bit = "";
		$out = "";
		$el = 0;
		$op = INFO_NOTHG_IND;
		for($i = 0; $i < strlen($str); $i++)
		{
			if($op == INFO_NOTHG_IND)
			{
				if($str[$i] == "#") $op = INFO_AS_IND;
				else if($str[$i] == "+") $op = INFO_NZ_IND;
				else if($str[$i] == "*") $op = INFO_IM_IND;
				else $out .= $str[$i];
			}
			else
			{
				if($str[$i] == "#" || $str[$i] == "+" || $str[$i] == "*")
				{
					if($bit == "_IM_") { $bit = \core\constFix("INFO_IM_BRAK"); $el--; }
					else if($bit == "_NZ_") {$bit = \core\constFix("INFO_NZ_BRAK"); $el--; }
					else if(strlen($bit) > 2 && $bit[0].$bit[1].$bit[2] == "_NR") $bit = \core\arabRoman(explode("_", $bit)[1] );
					
					if($textOnly)
						$out .= $bit;
					else
						switch($op)
						{
							case INFO_UPPER_IND:	$out .= "<span class='txtUpper'>".$bit."</span>";
													break;
							case INFO_CAPED_IND:	$out .= "<span class='txtCaped'>".$bit."</span>";
													break;
							default:				$out .= $bit;
						}
					$op = 0;
					$bit = "";
					$el++;
				}
				else $bit .= $str[$i];
			}
		}
		$out .= $bit;
		if($el < 2) $out .= "(".$this->personId.")";
		return $out;
	}
	public function addNpart($obj, $prefix, $queue, $visible = true) {
		$this->nParts[] = [$obj, $prefix, $queue, $visible];
	}
	
}
