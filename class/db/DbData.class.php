<?php
namespace db;
/**
 * Main Db-related class for communication and queries
 */
class DbData {
	public $objs = [];
	public $handler;
	public $cfg;
	
	private $db;
	private $db_name;
	
	public function __construct($cfg, $type = "user") {
		switch($type) {
			case "admin":	$user = DB_ADMN;
							$pass = DB_APSS;
							break;
			default:		$user = DB_LOGN;
							$pass = DB_PASS;
							break;
		}
		$this->db_name = DB_NAME;
		try {
			$this->db = mysqli_connect(DB_HOST, $user, $pass, $this->db_name);
			if(!$this->db) {
				throw new \Exception('Failed');
			}
		} catch(\Exception $e) {
			echo 'Server error. Please try again some time.';
			//die;
		}
		$this->cfg = $cfg;
		$lang_set = $this->getDbDataArray('setLang', [$cfg->lang->code] );
	}
	public function __destruct() {
		mysqli_close($this->db);
	}
	
	/**
	 * Return list of people related to object
	 * @param DbObj|null $obj  Object instance 
	 * @param mixed|null $type Optional data
	 * @return ListOfItems list of people
	 */
	public function getPeople($obj = null, $type = null) {
		if($obj === null)
			return $this->getDbData('getPeopleList', ["name"], OBJ_PERSON);
		switch($obj->getObjType() ) {
			case OBJ_PLACE:
						return $this->getDbData('getPlacePeople', [$obj->objId], OBJ_PERSON);
			case OBJ_EVENT:
						return $this->getDbData('getEventPeople', [$obj->objId, $type], OBJ_PERSON);
			case OBJ_IMAGE:
						return $this->getDbData('getPhotoPeople', [$obj->objId], OBJ_PERSON);
			case OBJ_SHIP:
						return $this->getDbData('getShipPeople', [$obj->objId], OBJ_PERSON);
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}

	/**
	 * Return list of children related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return ListOfItems list of children
	 */
	public function getChildren($obj = null) {
		if($obj === null)
			return new \core\ListOfItems($this->cfg);
		switch($obj->getObjType() ) {
			case OBJ_PLACE:
						//return $this->getDbData('getPlacePeople', [$obj->objId], OBJ_PERSON);
						return new \core\ListOfItems($this->cfg); // [?tmp -> subplace
			case OBJ_PERSON:
						return $this->getDbData('getChildren', [$obj->objId], OBJ_PERSON);
			case OBJ_SHIP:
						return $this->getDbData("getChildren", [$obj->getPeople()->getFirst()["obj"]->getId(), $obj->getPeople()->getLast()["obj"]->getId() ], OBJ_PERSON);
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}

	/**
	 * Return list of events related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return ListOfItems list of events
	 */
	public function getEvents($obj) {
		if($obj === null)
			return $this->getDbData('getEventList', [], OBJ_PERSON);
		switch($obj->getObjType() ) {
			case OBJ_PERSON:
						return $this->getDbData('getPersonEvents', [$obj->objId], OBJ_EVENT);
			case OBJ_PLACE:
						return $this->getDbData('getPlaceEvents', [$obj->objId], OBJ_EVENT);
			case OBJ_EVENT:
						return $this->getDbData('getEventsRelated', [$obj->objId], OBJ_EVENT);
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}

	/**
	 * Return list of images related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return ListOfItems list of images
	 */
	public function getImages($obj) {
		/*if($obj === null)
			return $this->getDbData('getEventList', [], OBJ_PERSON);*/
		switch($obj->getObjType() ) {
			case OBJ_PERSON:
						return $this->getDbData('getPersonPhotos', [$obj->objId], OBJ_IMAGE);
			case OBJ_PLACE:
						return $this->getDbData('getPlacePhotos', [$obj->objId], OBJ_IMAGE);
			case OBJ_EVENT:
						return $this->getDbData('getEventPhotos', [$obj->objId], OBJ_IMAGE);
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}
	
	/**
	 * Return list of links related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return ListOfItems list of links
	 */
	public function getLinks($obj) {
		switch($obj->getObjType() ) {	// [? do dbdataobj]
			case OBJ_PERSON:
						return $this->getDbDataArray('getPersonLinks', [$obj->objId] );
			case OBJ_PLACE:
						return $this->getDbDataArray('getPlaceLinks', [$obj->objId] );
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}

	/**
	 * Return list of places related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return ListOfItems list of places
	 */
	public function getPlaces($obj) {
		switch($obj->getObjType() ) {
			case OBJ_PERSON:
						return $this->getDbData('getPersonPlaces', [$obj->objId, "NULL", "NULL", 0], OBJ_PLACE);
			case OBJ_EVENT:
						return $this->getDbData('getEventPlaces', [$obj->objId], OBJ_PLACE);
			case OBJ_PLACE:
						return $this->getDbData('getSubPlaces', [$obj->objId], OBJ_PLACE);
			default:
						return new \core\ListOfItems($this->cfg);
		}
	}

	/**
	 * Return data from Db related to object
	 * @param DbObj|null $obj  Object instance 
	 * @return array data about object
	 */
	public function getData($obj) {
		switch($obj->getObjType() ) {
			case OBJ_PERSON:
						return $this->getDbDataArray('getPersonData', [$obj->objId] );
			case OBJ_EVENT:
						return $this->getDbDataArray('getEventData', [$obj->objId] );
			case OBJ_IMAGE:
						return $this->getDbDataArray('getPhotoData', [$obj->objId] );
			case OBJ_PLACE:
						return $this->getDbDataArray('getPlaceData', [$obj->objId] );
			case OBJ_SHIP:
						return $this->getDbDataArray('getShipData', [$obj->objId, 0, 0] );
			default:
						return [];
		}
	}
	
	/**
	 * Return return of predefined query to Db
	 * @param string $func name of MySQL stored function or procedure
	 * @param array $params list of params for stored function
	 * @param int $proc declare it's function (0) or prodecure (1)
	 * @see DbData::getDbData() OOP equivalent of this method
	 * @deprecated It is strongly recommended to use getDbData() method
	 * @return array two-dimmensional array contains result of query
	 */
	public function getDbDataArray($func, $params = [], $proc = 1) { // [? ]
		mysqli_next_result($this->db);
		$output = [];
		$paramString = "";
		foreach($params AS $p) {
			if($p === null || $p === '')
				$paramString .= ", NULL";
			else
				$paramString .= ",'".$p."'";
		}
		$paramString[0] = "(";
		$paramString .= ")";

		try {
			$q = mysqli_query($this->db, ($proc ? "CALL" : "SELECT")." ".$func.$paramString);
			$i = 0;
			while($q && $ans = mysqli_fetch_row($q) )
			{
				for($j = 0; $j < count($ans); $j++)
				{
					$output[$i][$j] = $ans[$j];
				}
				$i++;
			}
		}
		catch(\Exception $err) {
			$this->cfg->debug1 .= "<br />DB_ERR:".($proc ? "CALL" : "SELECT")." ".$func.$paramString."<br />";
		}

		return $output;
	}

	/**
	 * Return return of predefined query to Db
	 * @param string $func name of MySQL stored function or procedure
	 * @param array $params list of params for stored function
	 * @param string $obj_type type of DbObj object
	 * @param int $proc declare it's function (0) or prodecure (1)
	 * @return ListOfItems object of DbObj class or DbObj inherited class
	 */
	public function getDbData($func, $params, $obj_type, $proc = 1) {
		$objs = new \core\ListOfItems($this->cfg);
		mysqli_next_result($this->db);
		$paramString = "";
		foreach($params AS $p) {
			if($p === null || $p === '')
				$paramString .= ", NULL";
			else
				$paramString .= ",'".$p."'";
		}
		$paramString[0] = "(";
		$paramString .= ")";
		try {
			$this->cfg->debug1 .= "date:". ($proc ? "CALL" : "SELECT")." ".$func.$paramString;
			$q = mysqli_query($this->db, ($proc ? "CALL" : "SELECT")." ".$func.$paramString);
			while($ans = mysqli_fetch_assoc($q) ) {
				switch($obj_type) {
					case OBJ_PERSON:
								$obj = new DbPerson($this->cfg, reset($ans) );
								break;
					case OBJ_PLACE:
								$obj = new DbPlace($this->cfg, reset($ans) );
								break;
					case OBJ_EVENT:
								$obj = new DbEvent($this->cfg, reset($ans) );
								break;
					case OBJ_IMAGE:
								$obj = new DbImage($this->cfg, reset($ans) );
								break;
					case OBJ_NAME:
								$obj = new DbName($this->cfg, reset($ans) );
								break;
					case OBJ_NPART:
								$obj = new DbNpart($this->cfg, reset($ans), $ans["namePart"] );
								break;
					case OBJ_SHIP:
								$obj = new DbShip($this->cfg, reset($ans) );
								break;
					default:
								$obj = new DbObj($this->cfg, reset($ans) );
				}
				foreach($ans as $col => $val) {
					$obj->setPropExtra($col, $val);
					$obj->extra_txt[$col] = $val;	// [?depr
				}
				$objs->addItem($obj);
			}
		}
		catch(\Exception $err) {
			$this->cfg->debug1 .= "<br />DB_ERR:".($proc ? "CALL" : "SELECT")." ".$func.$paramString."<br />";
		}
		return $objs;
	}

}
