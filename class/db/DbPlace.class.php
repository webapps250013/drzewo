<?php
namespace db;

final class DbPlace extends DbObj {
	private $super;
	
	public function __construct($cfg, $id, $dummy = 0) {
		$this->cfg = $cfg;
		$this->obj_type = OBJ_PLACE;
		$this->color_prefix_general = UX_COLOR_PLACE;
		$db = $cfg->db;
		$this->objId = $id;
		if($this->objId == "") $this->objId = "0";
		if($dummy)
			return;
		if(count( $place = $this->getData() ) == 0) {
			exit("no place".$this->objId);
			return;
		}
		$this->subtype = $place[0][3];
		$this->super = new DbPlace($this->cfg, $place[0][5], 1);
		$this->names[] = $place[0][1];
		$this->image = new \core\UserImage($cfg, $this);
	}
	public function getSuperPlace() {
		return $this->super;
	}
}
