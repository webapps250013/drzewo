<?php
namespace db;

final class DbPrefix extends DbObj {
	private $str;
	
	public function __construct($cfg, $id, $str) {
		$this->cfg = $cfg;
		$this->objId = $id;
		$this->str = $str;
	}
}
