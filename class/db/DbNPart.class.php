<?php
namespace db;

final class DbNpart extends DbObj {
	
	private $str;
	private $master;
	
	public function __construct($cfg, $id, $str, $master = NULL) {
		$this->cfg = $cfg;
		$this->objId = $id;
		$this->str = $str;
		$this->master = $master;
	}
	public function getMaster() {
		return $this->master;
	}
	public function getString() {
		return $this->str;
	}
}

