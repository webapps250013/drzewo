<?php
namespace db;

final class DbImage extends DbObj {
	public function __construct($cfg, $id, $dummy = 0) {
		$this->cfg = $cfg;
		$this->objId = $id;
		$this->obj_type = OBJ_IMAGE;
		$this->color_prefix_general = UX_COLOR_IMAGE;
		
		if($dummy)
			return;
		if(count( $img = $this->getData() ) == 0) {
			$this->names[] = "Brak zdjęć"; // [?stałe ]
			$this->image = new \core\UserImage($cfg, $this, "0");
		}
		else {
			$this->names[] = $img[0][0];
			$this->image = new \core\UserImage($cfg, $this, $img[0][0], "jpg" );
		}
	}
}
