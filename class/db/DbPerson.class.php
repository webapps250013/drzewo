<?php
namespace db;

final class DbPerson extends DbObj {
	public $sex;
	public $father;
	public $mother;

	public $shipStatus;
	public $shipAmount;
	public $childAmount;
	
	public $ships = [];			// obiekt klasy ListOfItems | DbShip
	
	private $ids = [];	// obiekty klasy DbName
	
	public function __construct($cfg, $id, $namesObj = 0, $dummy = 0) {
		$this->cfg = $cfg;
		$this->objId = (int) $id;
		$this->obj_type = OBJ_PERSON;
		$this->color_prefix_general = UX_COLOR_PERSON;
		
		if($dummy) {
			$propNames = ["sex", "bDate", "dDate", "father", "mother", "mainName", "desc", "bio"];
			foreach($propNames AS $pName) {
				if(isset($this->propExtra[$pName] ) ) {
					
				}
			}
			$nms = $this->getNames();
			return;
		}
		if(count($person = $this->getData() ) == 0) {
			exit("no person");
			return;
		}
		$this->sex = $person[0][0];
		$this->desc = $person[0][10];
		$this->shipStatus = $person[0][1];
		$this->shipAmount = $person[0][2];
		$this->childAmount = $person[0][3];
		$this->father = new DbPerson($cfg, $person[0][6], 0, 1);
		$this->mother = new DbPerson($cfg, $person[0][7], 0, 1);
		
		$this->lifetime = new \core\Lifetime($person[0][4], $person[0][4], $person[0][5], $person[0][5] );
		if($this->lifetime->hasEnd() ) {
			$this->color_prefix_specific = UX_COLOR_DEAD;
			$this->status = 'dead';
		}
		else {
			$this->status = 'live';
		}

		$nms = $this->getNames();
		$this->image = new \core\UserImage($cfg, $this, reset($this->ids)->getNameString(1) );
	}

	public function getShip($partner) {
		if(count($this->ships) < 1) {
			$shipData = $this->cfg->db->getDbDataArray("getShipData", [0, $this->objId, 0] );
			foreach($shipData AS $ship) {
				$this->ships[] = new DbShip($this->cfg, $ship[0] );
			}
		}
		foreach($this->ships AS $ship) {
			$this->cfg->debug1 .= $this->getId()." -- ".$ship->getOtherPerson($this)->getId()." == ".$partner->getId()."<br />";
			if($ship->getOtherPerson($this)->getId() == $partner->getId() )
				return $ship;
		}
		return null;
	}
	public function getShips() {
		if(count($this->ships) < 1) {
			$shipData = $this->cfg->db->getDbDataArray("getShipData", [0, $this->objId, 0] );
			foreach($shipData AS $ship) {
				$this->ships[] = new DbShip($this->cfg, $ship[0] );
			}
		}
		return $this->ships;
	}
	
	public function getNames() {
		$this->ids = [];	// [?tmp]
		$parts = $this->cfg->db->getDbData("getIdentities", [$this->objId], OBJ_NPART);
		$lastId = 0;
		foreach($parts->getItems() as $pArray) {
			$pt = $pArray["obj"];	// [?tmp]
			if($lastId != $pt->extra_txt["nameId"] ) {
				$lastId = $pt->extra_txt["nameId"];
				$this->ids[] = new DbName($this->cfg, $lastId);
				end($this->ids)->nameString = $pt->extra_txt["nameString"];
				end($this->ids)->names[] = $pt->extra_txt["nameString"];
				if($pt->extra_txt["nameString"] == $pt->extra_txt["mainName"] )
					$this->names[] = end($this->ids)->getNameString();
			}
			end($this->ids)->addNpart($pt, $pt->extra_txt["prefix"], $pt->extra_txt["npartQueue"], $pt->extra_txt["npartVisible"] );
		}
		return $this->ids;
	}
}
