<?php
namespace db;

final class DbShip extends DbObj {
	
	public function __construct($cfg, $sid) {
		$this->objId = $sid;
		$this->cfg = $cfg;
		$db = $cfg->db;
		$this->obj_type = OBJ_SHIP;
		
		$shipData = $db->getDbDataArray("getShipData", [$sid, 0, 0] );
		if(count($shipData) < 1)
			return;
		$ppl = [];
		$ppl[] = new DbPerson($cfg, $shipData[0][1] );
		$ppl[] = new DbPerson($cfg, $shipData[0][2] );
		$this->people = new \core\ListOfItems($cfg, $ppl);
		$this->status = $shipData[0][11];
		$this->subtype = $shipData[0][12];
		$this->lifetime = new \core\Lifetime($shipData[0][7], $shipData[0][9], $shipData[0][8], $shipData[0][10] );
		
		$this->names[0] = reset($ppl)->getName()." — ".end($ppl)->getName();
		$this->names[reset($ppl)->getId() ] = reset($ppl)->getName();
		$this->names[end($ppl)->getId() ] = end($ppl)->getName();
		$this->names["person".GLOB_M] = $shipData[0][13];
		$this->names["person".GLOB_F] = $shipData[0][14];
	}
	public function getOtherPerson($p1) {
		foreach($this->people->getItems() AS $person) {
			if($person["obj"]->getId() !== $p1->getId() )
				return $person["obj"];
		}
		return [];
	}
}