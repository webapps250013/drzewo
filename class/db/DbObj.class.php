<?php
namespace db;

class DbObj {
	public $objId;
	public $image = null;	// obiekt klasy UserImage
	public $names = [];		// obiekty klasy DbName
	
	public $name = "";		// [?depr
	public $lifetime = null;	// docelowo protected; obiekt klasy Lifetime
	
	public $obj_type;

    public $extra_txt;      // [?depr
    public $desc;            // [?from DbPerson class / to inspect
	
	protected $propExtra = [];
	
	protected $cfg;
	protected $color_prefix_general = UX_COLOR_MAIN;
	protected $color_prefix_specific = null;
	
	protected $subtype = null;
	protected $status = null;
	
	protected $people = null;	// obiekt klasy ListOfItems
	protected $children = null;	// obiekt klasy ListOfItems
	protected $places = null;	// obiekt klasy ListOfItems
	protected $events = null;	// obiekt klasy ListOfItems
	
	public function __construct($cfg, $obj_type, $id) {
		$this->cfg = $cfg;
		$this->obj_type = $obj_type;
		$this->objId = $id;
	}
	
	public function getName($key = null) {
		if($key === null)
			return reset($this->names);
		else
			return $this->names[$key];
	}
	public function getId() {
		return $this->objId;
	}
	public function getImg() {
		if($this->image === null)
			return (new \core\UserImage($this->cfg, $this) );
		else
			return $this->image;
	}
	public function getTile() {
		switch($this->getObjType() ) {
			case OBJ_PERSON:
						$tile = new \tile\TilePerson($this->cfg, $this);
						break;
			case OBJ_EVENT:
						$tile = new \tile\TileEvent($this->cfg, $this);
						break;
			case OBJ_PLACE:
						$tile = new \tile\TilePlace($this->cfg, $this);
						break;
			default:
						$tile = new \tile\Tile($this->cfg, $this);
		}
		
		return $tile->draw();
	}
	public function getObjType() {
		return $this->obj_type;
	}
	public function getColorPrefix($specific = false) {
		if($specific && $this->color_prefix_specific === null) {
			$this->color_prefix_specific = $this->color_prefix_general;
			return $this->color_prefix_specific;
		}
		else if($specific) {
			return $this->color_prefix_specific;
		}
		else {
			return $this->color_prefix_general;
		}
	}
	public function getStatus() {
		return $this->status;
	}
	public function getSubtype() {
		return $this->subtype;
	}
	public function getPropExtra($pName) {
		if(isset($this->propExtra[$pName] ) )
			return $this->propExtra[$pName];
		else
			return "";
	}
	
	public function setPropExtra($pName, $pVal) {
		$this->propExtra[$pName] = $pVal;
	}
	
	
	public function getPeople($type = null) {
		$this->cfg->debug1 .= "null:".is_null($this->people)."//";
		if(is_null($this->people) )
			$this->people = $this->cfg->db->getPeople($this, $type);
		return $this->people;
	}
	public function getChildren() {
		if(is_null($this->children) )
			$this->children = $this->cfg->db->getChildren($this);
		return $this->children;
	}
	public function getEvents() {
		if(is_null($this->events) )
			$this->events = $this->cfg->db->getEvents($this);
		return $this->events;
	}
	public function getImages() {
		return $this->cfg->db->getImages($this);
	}
	public function getLinks() {
		return $this->cfg->db->getLinks($this);
	}
	public function getPlaces() {
		if(is_null($this->places) )
			$this->places = $this->cfg->db->getPlaces($this);
		return $this->places;
	}
	public function getData() {
		return $this->cfg->db->getData($this);
	}
}
