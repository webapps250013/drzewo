<?php
namespace db;

final class DbEvent extends DbObj {
	public function __construct($cfg, $id, $dummy = 0) {
		$this->cfg = $cfg;
		$this->obj_type = OBJ_EVENT;
		$this->color_prefix_general = UX_COLOR_EVENT;
		$this->objId = $id;
		
		if($dummy)
			return;
		if(count( $event = $this->getData() ) == 0) {
			return;
		}
		$this->names[] = $event[0][0];
		$this->names[] = $event[0][0]." (".$this->getPeople(1)->draw("", LTYPE_INLINE).")";
		$this->image = new \core\UserImage($cfg, $this, $event[0][6] );
		$this->lifetime = new \core\Lifetime($event[0][4], $event[0][4], $event[0][5], $event[0][5] );
		if($this->lifetime->hasEnd() )
			$this->status = 'ended';	// [?tmp]
		else
			$this->status = 'still';
	}
}
