<?php
namespace db;

final class DbLink extends DbObj {
	
	private $dpx;	// domain prefix
	private $dsx;	// domain sufix
	private $lnk;	 
	private $ttl;	 
	private $txt;	
	private $mtd;	// form method
	private $prt;	// protocol
	
	public function __construct($cfg, $id) {
		$this->cfg = $cfg;
		$this->objId = $id;
	}
	public function makeLink($dpx, $dsx, $lnk, $ttl, $txt, $method = "GET", $prt = "https") {
		$output = "";
		if($method == "POST") {
			$attr = explode("&", $lnk);
			$output .= '<form action="'.$prt."://".$dpx.$dsx.'" method="POST" class="postLink" target="_blank">';
			for($i = 0; $i < count($attr); $i++) {
				$vals = explode("=", $attr[$i] );
				if(!isset($vals[1] ) ) $vals[1] = $vals[0];
				$output .= '<input type="hidden" name="'.$vals[0].'" value="'.$vals[1].'" />';
			}
			$output .= '</form>
				<span onclick="this.previousElementSibling.submit()" class="postLinkSubmit" title="'.$ttl.'">'.$txt.'</span>';
		}
		else {
			$output .= "<a target='_blank' href='".$prt."://".$dpx."/".$lnk.$dsx."' title='".$ttl."'>".$txt."</a>";
		}
		return $output;
	}
}
