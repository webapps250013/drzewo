<?php
namespace tile;

class Tile {
	public $tileSide = [];	// [DbObj]
	public $tileImage;		// obiekt UserImage
	public $tileLine1;		// string
	public $tileLine2;		// string
	public $tileLine3;		// string
	public $tileButton = []; // [STRING, LINK_PARAM, LINK_VAL]
	
	public $tileType = "ib";
	
	protected $tileParams = ["i", "b"];
	/* 0: i - info, p - parent, c - children */
	/* 1: b - block, i - inline */
	
	protected $cfg;
	protected $colorPrefix = UX_COLOR_MAIN;
	protected $mainObj = null;

	public function __construct($colorPrefix = UX_COLOR_MAIN) {
		$this->colorPrefix = $colorPrefix;
	}

	public function draw() {
		$out = "<div class='tileContainer '".($this->tileType[1] == "i" ? "style='display:inline-block;'" : "").">";
		$out .= "<div class='tileSide'>";
		foreach($this->tileSide AS $side) {
			$out .= "<a href='index.php?".$side->getObjType()."=".$side->getId()."'>"
						.$side->getOtherPerson($this->mainObj)->getImg()->draw("TILE-SIDE")
					."</a>";
		}
		$out .= "</div>";
		$out .= "<a href='index.php?".$this->mainObj->getObjType()."=".$this->mainObj->getId()."'>"
					.$this->tileImage->draw()
					."<div class='tileTxt ".$this->colorPrefix."ColorT'>"
						.$this->tileLine1."<br />"
						.$this->tileLine2."<br />"
						.$this->tileLine3
					."</div>"
				."</a>";
		
		foreach($this->tileButton AS $btt) {
			switch($btt[1] ) {
				case 'INACTIVE':
					$out .= "<div class='tileLink bttInactive'>".$btt[0]."</div>";
					break;
				case 'HIDDEN':
					$out .= "<div class='tileLink bttHidden'> </div>";
					break;
				default:
					$out .= "<div class='tileLink bttActive'><a href='index.php?".$btt[1]."=".$btt[2]."'>".$btt[0]."</a></div>";
			}
		}
		$out .=	"</div>";
		
		return $out;
	}
	
	protected function init($cfg, $obj, $objType, $objClass, $type, $colorSpecific = false) {
		$this->cfg = $cfg;
		$objClass = '\db\\'.$objClass;
		
		while(strlen($type) < 2) {
			$type .= $this->tileParams[strlen($type) ];
		}
		$this->tileType = $type;
		
		if(is_object($obj) && $obj->getObjType() != $objType) {
			$obj = new $objClass($cfg, 0);
		}
		else if(!is_object($obj) ) {
			$obj = new $objClass($cfg, $obj);
		}
		$this->mainObj = $obj;
		
		$this->colorPrefix = $this->mainObj->getColorPrefix($colorSpecific);
		$this->tileImage = $this->mainObj->getImg("TILE");
		
		return $this->mainObj;
	}
}
