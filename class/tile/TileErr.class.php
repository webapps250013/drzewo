<?php
namespace tile;

final class TileErr extends Tile {
	
	public function __construct($cfg, $cnt = ERR_002_O, $header = ERR_002_T) {
		$this->cfg = $cfg;
		$this->mainObj = new \db\DbObj($this->cfg, OBJ_GENERIC, 0);
		
		$this->tileImage = new \core\UserImage($this->cfg, new \db\DbObj($cfg, OBJ_EVENT, 0) );
		$this->tileLine1 = $header;
		$this->tileLine2 = \core\constFix("ERR_T00_O");
		$this->tileLine3 = $cnt;
	}
}