<?php
namespace tile;

final class TileEvent extends Tile {
	
	public function __construct($cfg, $event, $type = "ib") {
		$event = $this->init($cfg, $event, OBJ_EVENT, "DbEvent", $type);

		$evPpStr = $this->mainObj->getPeople(1)->draw('', LTYPE_INLINE);
		
		$this->tileLine1 = $evPpStr;									// lista osób PERSON_1
		$this->tileLine2 = $this->mainObj->getName();					// typ wydarzenia
		$this->tileLine3 = $this->mainObj->lifetime->getLength(0, 1);	// data wydarzenia
	}
}
