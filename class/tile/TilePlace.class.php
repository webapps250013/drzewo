<?php
namespace tile;

final class TilePlace extends Tile {
	
	public function __construct($cfg, $place, $type = TILE_TYPE_INFO) {
		$place = $this->init($cfg, $place, OBJ_PLACE, "DbPlace", $type);
		
		$this->tileLine1 = $this->mainObj->getName();
		$this->tileLine2 = $this->mainObj->getSubtype();
		$this->tileLine3 = $this->mainObj->getSuperPlace()->getName();
	}
}
