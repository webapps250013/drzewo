<?php
namespace tile;

final class TilePerson extends Tile {
	
	public function __construct($cfg, $person, $type = "ib", $shipId = 0) {
		$person = $this->init($cfg, $person, OBJ_PERSON, "DbPerson", $type, true);
		
		if( ($residence = $this->cfg->db->getDbData('getPersonPlaces', [$this->mainObj->getId(), 10, ($type[0] == "p" ? "2" : "1"), 1], OBJ_PLACE )
				)->getAmount() == 0)	// [?] zamienić na stałe
			$residence = new \core\ListOfItems($this->cfg, [new \db\DbPlace($this->cfg, 0, 0) ] );
		
		if($type[0] == "p") {
			if(count($shp = $person->getShips() ) > 1 ) {
				foreach($shp as $s) {
					if($s->getId() != $shipId)
						$this->tileSide[] = $s;
				}
			}
		}
		
		$this->tileLine1 = $person->getName();
		$this->tileLine2 = $person->lifetime->getLength(2, 1);
		$this->tileLine3 = "<span class='placeFormat placeColorT'>".$residence->getFirst()["obj"]->getName()."</span>";
		
		if($type[0] != "i") {
			if($type[0] == "p") {
				if( is_object($fObj = $person->father->getShip($person->mother) ) ) {
					$this->tileButton[] = [$fObj->getName($person->father->getId() ), OBJ_SHIP, $fObj->getId() ];
				}
				else {
					$this->tileButton[] = ["", "HIDDEN", 0];
				}
			}
			else {
				foreach($person->getShips() AS $ship) {
					$this->tileButton[] = [$ship->getName($ship->getOtherPerson($person)->getId() ), OBJ_SHIP, $ship->getId() ];
				}
				if(count($this->tileButton) == 0 && $person->shipStatus == "1" /* brak danych */ )
					$this->tileButton[] = [SHIPS_NO_DATA, "INACTIVE", $person->getId() ];
				else if(count($this->tileButton) == 0 && $person->shipStatus == "2" /* celibat */ )
					$this->tileButton[] = [SHIPS_CELIBACY, "INACTIVE", $person->getId() ];
				else if(count($this->tileButton) == 0)
					$this->tileButton[] = ["", "HIDDEN", 0];
			}
		}
	}
}
