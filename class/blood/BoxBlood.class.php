<?php
namespace blood;

final class BoxBlood extends \box\Box {
	public function __construct($cfg, $person1, $person2 = null, $aff = 0) {
		$person1 = $this->init($cfg, $person1, OBJ_PERSON, "DbPerson");

		if($person2 === null )
			$person2 = $person1;
		else if(!is_object($person2) )
			$person2 = new \db\DbPerson($cfg, $person2);

		$headerStatusLine[0] = "";
		$headerStatusLine[1] = "";
		
		$blood = new Blood($cfg, $person1, $person2);
		$rce = $blood->getRelatives();

		if(count($rce) == 0) {
			$rce[count($rce) ]["name"]["p1"] = UX_BOX_BLOOD_NO_BLOOD;
			$rce[count($rce)-1 ]["name"]["p2"] = UX_BOX_BLOOD_NO_BLOOD;
		}
		for($i = 0; $i < count($rce); $i++)
			$headerStatusLine[0] .= $rce[$i]["name"]["p1"]."<br/>";
		
		for($i = 0; $i < count($rce); $i++)
			$headerStatusLine[1] .= $rce[$i]["name"]["p2"]."<br/>";
		
		$pList = $cfg->db->getPeople();

		// $contentGraph = "<table class='boxContentTableGraph'>".$graphContent."</table>";
		$contentSelRel =
				"<form method='GET' name='krewFormR' id='krewFormR' class='boxContentFormRelative'>
					<p class='desc'>".UX_BOX_BLOOD_DESC_FROM_BLOOD."</p>"
					.$pList->draw("", LTYPE_SELECT_GENERIC)		// ["name" => ATTR_BLOOD],		["selected" => $id] )
					.$pList->draw("", LTYPE_SELECT_GENERIC)		// ["name" => ATTR_BLOOD."2"],	["selected" => $id2] )
					."<input type='hidden' name='".ATTR_BLOOD."3' id='krewFormTyp' value='0' />"
				."</form>";
		$contentSelAff =		
				"<form method='GET' name='krewFormA' id='krewFormA' class='boxContentFormAffinity'>
					<p class='desc'>".UX_BOX_BLOOD_DESC_FROM_LAW."</p>"
					.$pList->draw("", LTYPE_SELECT_GENERIC)		// ["name" => ATTR_BLOOD],		["selected" => $id] )
					.$pList->draw("", LTYPE_SELECT_GENERIC)		// ["name" => ATTR_BLOOD."2"],	["selected" => $id2] )
					."<input type='hidden' name='".ATTR_BLOOD."3' id='krewFormTyp' value='1' />"
				."</form>";
		
		$this->boxHeader->addObj($person1->getName(), "", "", $headerStatusLine[0], $person1->image->draw("BOX") );
		$this->boxHeader->addObj($person2->getName(), "", "", $headerStatusLine[1], $person2->image->draw("BOX"), "right");
		
		$this->boxContent->addParagraph("boxContentFormRelative",	\core\constFix("PKR_KREW"),				$contentSelRel);
		$this->boxContent->addParagraph("boxContentFormAffinity",	\core\constFix("PKR_PRAWO"), 				$contentSelAff);
		// $this->boxContent->addParagraph("boxContentTableGraph",		constFix("UX_LIST_LABEL_GRAPH"),	$contentGraph);
		
		$this->boxButtons->setLeftButton(UX_BOX_BLOOD_BTT_LEFT, "graph/plansza_js.php?id=".$person1->getId() );
		$this->boxButtons->setRightButton(UX_BOX_BLOOD_BTT_RIGHT, "", "document.getElementById(&quot;krewForm&quot;).submit()");
	}
}