<?php
namespace blood;

class Blood {
	public $relatives = [];
	public $affinity = [];
	
	private $cfg;
	private $person1 = null;
	private $person2 = null;
	
	public function __construct($cfg, $p1, $p2) {
		$this->cfg = $cfg;
		$this->person1 = $p1;
		$this->person2 = $p2;
	}
	public function getRelatives() {
		if(count($this->relatives) < 1)
			$this->bloodFromBlood();
		return $this->relatives;			
	}
	public function getAffinity() {
		if(count($this->affinity) < 1)
			$this->bloodFromLaw();
		return $this->affinity;
	}

	private function bloodAsc($proband) {	// dopracować [?]
		$db = $this->cfg->db;
		$tree[0][0] = $proband;
		
		///////////////////////////////// [? indev]
		
		return $tree;
	}
	private function bloodDesc($proband) {

		$tree[0][0][0] = null;

		///////////////////////////////// [? indev]

		return $tree;
	}
	private function bloodFromBlood() {
		$p1 = $this->person1;
		$p2 = $this->person2;
		
		$rels = [];

		///////////////////////////////// [? indev]

		$this->relatives = $rels;
	}
	private function bloodFromLaw() {
		/*	powinowactwo

		ojczym / macocha			późniejszy partner rodzica				
		pasierb / pasierbica		dziecko partnera z poprzednich związków
		przysposobiony				adoptowane dziecko							// EVENT_PRIM_TYPE = ADOPTION
		
		przybrany brat / siostra	przybrane rodzeństwo
		
		teść / teściowa				rodzic partnera; związek formalny
		zięć / synowa				partner dziecka; związek formalny
		
		swat / swatowa				rodzice partnera dziecka; związek fromalny
		
		szwagier / szwagierka		brat/siostra partnera; związek formalny
		szwagier / bratowa			partner siostry/brata; związek formalny
		
		PARTNER_M / PARTNER_K		małżeństwo, konkubinat, nażeczeństow, etc.; związek formalny/półformalny
		*/
		$db = $this->cfg->db;
		$affinity = [];
		
		///////////////////////////////// [? indev]
		
		$this->affinity = $affinity;
	}
}
