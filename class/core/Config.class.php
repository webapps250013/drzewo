<?php
namespace core;

class Config {
	public $lang = null;
	public $db = null;
	public $modules = [];
	public $def_box = [];
	public $def_cnt = [];
	public $def_rbb = [];
	public $def_ftr = [];
	public $def_stl = [];
	
	public $debug1 = "";	// treść okna debugowania 1.
	public $debug2 = "";	// treść okna debugowania 2.
		
	public function __construct($db_mode = "user") {
		if(file_exists("config.php") ) {
			include_once("config.php");
		}
		else {
			exit("ERR: no config. Try to install file.");
		}
		
		include_once("module/_loader.php");
		$this->lang = new UserLang($this);
		
		include_once("class/db/DbData.class.php");
		$this->db = new \db\DbData($this, $db_mode);
	}
	
	public function constFix($constName, $alt = null) {
		if($alt === null) $alt = $constName;
		return (defined($constName) ? constant($constName) : $alt);
	}
	
	public function addBoxDef($m_const, $o_const, $method, $fcn, $dummy = [] ) {
		$this->def_box[] = [$m_const, $o_const, $method, $fcn, $dummy];
	}
	public function addCntDef($array) {
		$this->def_cnt[] = $array;
	}
	public function addStyleDef($str) {
		$this->def_stl[] = $str;
	}
	public function addRibbonItemDef($item) {
		$this->def_rbb[] = $item;
	}
	public function addFooterItemDef($item) {
		$this->def_ftr[] = $item;
	}
}
