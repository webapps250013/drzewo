<?php

namespace core;

class Lifetime {
	private $date1s;	// [Y, M, D, C] — rok, miesiąc, dzień, do obliczeń
	private $date1e;
	
	private $date2s;
	private $date2e;
	
	private $date1;
	private $date2;
	
	public function __construct($date1s, $date1e, $date2s, $date2e) {
		if($date1s == NULL || $date1s == "")
			$date1s = UX_DATE_FLAG_DAY;
		if($date1e == NULL || $date1e == "")
			$date1e = UX_DATE_FLAG_DAY;
		if($date2s == NULL || $date2s == "")
			$date2s = UX_DATE_FLAG_NOW;
		if($date2e == NULL || $date2e == "")
			$date2e = UX_DATE_FLAG_NOW;
		
		$this->date1s = $this->dateDecode($date1s);
		$this->date1e = $this->dateDecode($date1e);
		$this->date2s = $this->dateDecode($date2s);
		$this->date2e = $this->dateDecode($date2e);		
	}
	
	public function getDateStart($order = 0, $pattern = 1, $raw = 0) {
		return ($raw ? $this->date1s : $this->dateFix($this->date1s, $this->date1e, $order, $pattern) );
	}
	
	public function getDateEnd($order = 0, $pattern = 1, $raw = 0) {
		return ($raw ? $this->date2e : $this->dateFix($this->date2s, $this->date2e, $order, $pattern) );
	}
	
	public function getLength($order = 0, $pattern = 1) {
		return $this->dateFix($this->date1s, $this->date2e, $order, $pattern);
	}
	
	public function isExactDate($type, $flag = UX_DATE_FLAG_DAY) {
		if($type == '1') {
			$dateS = $this->date1s;
			$dateE = $this->date1e;
		}
		else {
			$dateS = $this->date2s;
			$dateE = $this->date2e;
		}
		if($dateS["c"] != $dateE["c"] )
			return false;
		
		switch($flag) {
			case UX_DATE_FLAG_YEAR:
					if($dateS["f"] == $flag)
						return true;
			case UX_DATE_FLAG_MONTH:
					if($dateS["f"] == UX_DATE_FLAG_MONTH || $dateS["f"] == UX_DATE_FLAG_YEAR)
						return true;
			case UX_DATE_FLAG_DAY:
					if($dateS["f"] == UX_DATE_FLAG_DAY || $dateS["f"] == UX_DATE_FLAG_MONTH || $dateS["f"] == UX_DATE_FLAG_YEAR)
						return true;
			default:
					return false;
		}
	}
	public function isAdult() {
		if($this->date2e["y"] - $this->date1s["y"] > GLOB_ADULT_AGE)
			return true;
		else
			return false;
	}
	public function isConsentAge() {
		if($this->date2e["y"] - $this->date1s["y"] > GLOB_CONSENT_AGE)
			return true;
		else
			return false;
	}
	public function hasEnd() {
		if($this->date2e["f"] == UX_DATE_FLAG_NOW)
			return false;
		else
			return true;
	}
	
	
	public function dateDecode($ymdf) {
		$day = (int) ( (int) $ymdf / 10);	
		$flag = (int) $ymdf - ($day."0");	
		$mth = (int) ( (int) $day / 100);	
		$day = (int) $day - ($mth."00");	
		$year = (int) ($mth / 100);			
		$mth = $mth - ($year."00");			
		
		if($flag == UX_DATE_FLAG_NOW) {
			$year = date("Y");
			$mth = date("m");
			$day = date("d");
			$ymdf = $year.$mth.$day.$flag;
		}
		
		return ["y" => $year, "m" => $mth, "d" => $day, "f" => $flag, "c" => $ymdf];
	}
	public function dateCompose($year, $mth, $day, $flag, $pttrn) {
		$output = "";
		$sx = "";
		$px = "";
		if($year < 0) {
			$sx = UX_DATE_BCE_SX;
		}
		$el["d"] = ($day < 1 || $day > 31 ? UX_DATE_BD.UX_DATE_BD : $day );
		$el["D"] = ($day < 1 || $day > 31 ? UX_DATE_BD.UX_DATE_BD : $day );
		$el["m"] = ($mth < 1 || $mth > 12 ? UX_DATE_BD.UX_DATE_BD : $mth );	
		$el["M"] = ($mth < 1 || $mth > 12 ? UX_DATE_BD.UX_DATE_BD : $mth );	
		$el["X"] = arabRoman12($el["m"] );
		$el["s"] = constFix("UX_DATE_MTH_SHRT_".$mth, $mth);
		$el["S"] = constFix("UX_DATE_MTH_STR_".$mth, $mth);
		$el["Y"] = (string) ($year == 0 ? UX_DATE_BD.UX_DATE_BD.UX_DATE_BD.UX_DATE_BD : $year );
		switch($flag) {
			case '0':	//before
						$px .= UX_DATE_BEFORE_PX." ";
						$sx .= " ".UX_DATE_BEFORE_SX;
						break;
			case '1':	//millenium
						$px .= UX_DATE_MILL_PX." ";
						$sx .= " ".UX_DATE_MILL_SX;
						break;
			case '2':	//century
						$px .= UX_DATE_CENT_PX." ";
						$sx .= " ".UX_DATE_CENT_SX;
						break;
			case '3':	//circa
						$px .= UX_DATE_CIRCA_PX." ";
						$sx .= " ".UX_DATE_CIRCA_SX;
						$el["d"] = "";
						$el["m"] = "";
						$el["M"] = "";
						break;
			case '4':	//year
						
						break;
			case '5':	//month
						
						break;
			case '6':	//day
						
						break;
			case '7':	//between
						
						break;
			case '8':	//after
						$px .= UX_DATE_AFTER_PX." ";
						$sx .= " ".UX_DATE_AFTER_SX;
						break;
			default:	//now
						
		}
		for($i = 0; $i < strlen($pttrn); $i++)
		{
			if( isset($el[$pttrn[$i] ] ) ) $output .= $el[$pttrn[$i] ];
				else $output .= $pttrn[$i];
		}
		return $px.$output.$sx;
	}
	private function dateFix($date1, $date2, $order, $rmd = 0) {	//[? STAŁE]
		$output = "";

		$prePttrn = [];
		$spt = UX_DATE_SEPARATOR;
		switch(UX_DATE_FORMAT)
		{
			// YMD, dmY, DMY, mdY, MDY, dSY, dsY, dXY
			case "1":	$prePttrn["y"] = "Y".$spt;
						$prePttrn["m"] = "M".$spt;
						$prePttrn["d"] = "D";
						break;
			case "2":	$prePttrn["d"] = "d".$spt;
						$prePttrn["m"] = "m".$spt;
						$prePttrn["y"] = "Y";
						break;
			case "3":	$prePttrn["d"] = "D".$spt;
						$prePttrn["m"] = "M".$spt;
						$prePttrn["y"] = "Y";
						break;
			case "4":	$prePttrn["m"] = "m".$spt;
						$prePttrn["d"] = "d".$spt;
						$prePttrn["y"] = "Y";
						break;
			case "5":	$prePttrn["m"] = "M".$spt;
						$prePttrn["d"] = "D".$spt;
						$prePttrn["y"] = "Y";
						break;
			case "6":	$spt = " ";
						$prePttrn["d"] = "d".$spt;
						$prePttrn["m"] = "s".$spt;
						$prePttrn["y"] = "Y";
						break;
			case "7":	$spt = " ";
						$prePttrn["d"] = "d".$spt;
						$prePttrn["m"] = "S".$spt;
						$prePttrn["y"] = "Y";
						break;
			default:	$spt = " ";
						$prePttrn["d"] = "d".$spt;
						$prePttrn["m"] = "X".$spt;
						$prePttrn["y"] = "Y";
		}

		// order -> ymd	ym	y	md	m	d
		//			0	1	2	3	4	5
		if($order > 2)
			$prePttrn["y"] = "";
		if($order == 2 || $order == 5)
			$prePttrn["m"] = "";
		if($order == 1 || $order == 2 || $order == 4)
			$prePttrn["d"] = "";
						
		$pttrn = "";
		foreach($prePttrn as $v)
			$pttrn .= $v;
		
		if($rmd 
			&& $date1["y"] != 0
			&& $date2["y"] != 0
			&& ($date1["y"] - $date2["y"] ) != 0
			&& ($date1["f"] == UX_DATE_FLAG_DAY || $date1["f"] == UX_DATE_FLAG_MONTH || $date1["f"] == UX_DATE_FLAG_YEAR || $date1["f"] == UX_DATE_FLAG_NOW)
			&& ($date2["f"] == UX_DATE_FLAG_DAY || $date2["f"] == UX_DATE_FLAG_MONTH || $date2["f"] == UX_DATE_FLAG_YEAR || $date2["f"] == UX_DATE_FLAG_NOW)
			)
				$output .= "&nbsp;[".abs($date2["y"] - $date1["y"] )."]";
		
		$out1 = $this->dateCompose($date1["y"], $date1["m"], $date1["d"], $date1["f"], $pttrn);
		$out2 = $this->dateCompose($date2["y"], $date2["m"], $date2["d"], $date2["f"], $pttrn);
		
		if($date2["f"] != UX_DATE_FLAG_NOW && $out1 != $out2 )
			$output = $out1." &ndash; ".$out2.$output;
		else
			$output = $out1.$output;
		
		return $output;
	}
}
