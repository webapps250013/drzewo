<?php
namespace core;

class Container {
	private $ribbon;
	private $footer;
	private $mainCnt;
	private $boxes = [];
	private $notes = [];
	private $cfg;
	
	public function __construct($cfg, $ribbon = null, $footer = null, $mainCnt = "") {
		$this->ribbon = ($ribbon === null ? new Ribbon($cfg) : $ribbon);
		$this->footer = ($footer === null ? new Footer($cfg) : $footer);
		$this->mainCnt = $mainCnt;
		$this->cfg = $cfg;
	}
	
	public function draw() {
		$out = '<body '.(isset($_GET[OBJ_IMAGE] ) ? "onload='imageMap()'" : "").' >';
		$out .= $this->ribbon->draw()
				.'<script type="application/javascript" src="'.GLOB_DIR_JS.'/core.js"></script>'
				.'<div id="container" class="mainColorT">';
		foreach($this->cfg->def_cnt AS $cnt) {
			$this->cfg->debug2 .= $cnt[1]."<br/>";
			$match = false;
			if(inputFix($cnt[2], $cnt[0] ) ) {
				include($cnt[1] );
				$match = true;
				break;
			}
		}
		if(!$match)
			include_once("cnt/main.php");
		$bxs = "";
		foreach($this->boxes AS $box) {
			if($box !== null)
				$bxs .= $box->draw();
			else
				$bxs .= "box is null<br />";
		}
		if(count($this->boxes) > 0)
			$out .= "<div class='boxBg' id='infoBlok'>".$bxs.'</div>';
		$out .= '</div>';
		$out .= $this->footer->draw();
		foreach($this->notes AS $note)
			$out .= $note->draw();
		$out .= '</body>';
		return $out;
	}
	public function addBox($box_obj) {
		$this->boxes[] = $box_obj;
	}
}
