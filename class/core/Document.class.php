<?php
namespace core;

class Document {
	public $head;
	public $container;
	
	private $cfg;
	
	public function __construct($cfg, $title = UX_APPNAME) {
		$this->head = new DocHead($cfg, $title);
		$this->container = new Container($cfg);
		$this->cfg = $cfg;
	}
	public function draw() {
		return $this->head->draw()
				.$this->container->draw();
	}
}
