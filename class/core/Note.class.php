<?php
namespace core;

class Note {
	private $color_h_prefix;
	private $color_i_prefix;
	private $h_icon;
	private $h_txt;
	private $c_txt;
	
	public function __construct($c_txt, $h_txt, $type = "NOTE") {
		$this->h_txt = $h_txt;
		$this->c_txt = $c_txt;
		switch($type) {
			case "FATAL":
					$this->color_i_prefix = "error";
					$this->color_h_prefix = UX_COLOR_MAIN;
					$this->h_icon = "!!";
					break;
			case "ERROR":
					$this->color_i_prefix = UX_COLOR_MAIN;
					$this->color_h_prefix = UX_COLOR_MAIN;
					$this->h_icon = "!";
					break;
			case "NOTE":
			default:
					$this->color_i_prefix = UX_COLOR_MAIN;
					$this->color_h_prefix = UX_COLOR_MAIN;
					$this->h_icon = "*";
		}
	}
	
	/**
	 * Drawing HTML code of note
	 * @return string
	 */
	public function draw() {
		return
			"<div class='note-container' style='position:fixed; bottom:0; left:0; width:100%; z-index:999; display:flex;'>"
				."<div style='flex:1;' class='note-icon".$this->color_i_prefix."Color'>".$this->h_icon."</div>"
				."<div style='flex:2; color:white;' class='note-head ".$this->color_h_prefix."Color'>".$this->h_txt."</div>"
				."<div style='flex:8; background-color:white;' class='note-content'>".$this->c_txt."</div>"
				//."<div class='note-btt'>"."</div>"
				."<div style='flex:1;' class='boxBttX' onclick='displayType(this.parentElement)'>".UX_BTT_CLOSE."</div>"
			."</div>";
	}
}
