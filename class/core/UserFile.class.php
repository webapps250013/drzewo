<?php
namespace core;

class UserFile {
	protected $cfg;
	protected $src;
	protected $fnm;
	protected $ext;
	protected $dft = null;
	
	public function __construct($cfg, $src, $fnm, $ext) {
		$this->cfg = $cfg;
		$this->src = $src;
		$this->fnm = $fnm;
		$this->ext = $ext;
	}
	
	public function setDefault($src, $fnm, $ext) {
		$dft = $src."/".$fnm.".".$ext;
		return ( file_exists($dft) ? $this->dft = $dft : false);
	}
	public function getFileString() {
		return $this->fileFix();
	}
	
	protected function fileFix() {
		if($this->dft == null) $this->dft = $this->fnm;
		$file_name = $this->src."/".$this->fnm.".".$this->ext;
		// $this->cfg->debug1 .= "<br />filefix:".$file_name."<br />";
		return ( file_exists($file_name) ? $file_name : $this->dft);
	}
}
