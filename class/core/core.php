<?php
namespace core;

/**
 * Return Roman numeral equivalent to Arab numeral input
 * @see arabRoman12() switch statement equivalent for months only (1-12)
 * @param mixed $arab Arab numeral
 * @return string Roman numeral
 */
function arabRoman($arab) {
	$roman = "";
	$table =	[
					["", "M", "MM", "MMM", "(4M)", "(5M)", "(6M)", "(7M)", "(8M)", "(9M)"],
					["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],
					["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
					["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
				];
	$hops = [1000, 100, 10, 1];
	$a1 = (int) $arab;
	if($a1 >= $hops[0] * 10) return $arab;
	for($i = 0; $i < 4; $i++)
	{
		$rm = (int) ($a1 / $hops[$i] );
		$roman .= $table[$i][$rm];
		$a1 -= $hops[$i] * $rm;
	}
	return ($roman == "" ? $arab : $roman);
}

/**
 * Return Roman numeral equivalent to Arab numeral input.
 * It can return 12 predefined Roman numerals (i.e. months)
 * @see arabRoman() Function equivalent for all Roman numerals
 * @param mixed $arab Arab numeral
 * @return string Roman numeral
 */
function arabRoman12($arab) {
	switch ( (int) $arab)
	{
	  case  1 : return "I";
	  case  2 : return "II";
	  case  3 : return "III";
	  case  4 : return "IV";
	  case  5 : return "V";
	  case  6 : return "VI";
	  case  7 : return "VII";
	  case  8 : return "VIII";
	  case  9 : return "IX";
	  case 10 : return "X";
	  case 11 : return "XI";
	  case 12 : return "XII";
	  default : return $arab;
	}
}


function constFix($constName, $alt = null) {
	if($alt == null) $alt = $constName;
	return (defined($constName) ? constant($constName) : $alt);
}
function inputFix($prtcl = "GET", $str, $alt = null) {
	if($alt === null)
		$out = '';
	else
		$out = $alt;
	switch($prtcl) {
		case "GET":
					if(isset($_GET[$str] ) )
						$out = htmlentities($_GET[$str], ENT_HTML5);
					break;
		case "POST":
					if(isset($_POST[$str] ) )
						$out = htmlentities($_POST[$str], ENT_HTML5);
					break;
		case "COOKIE":
					if(isset($_COOKIE[$str] ) )
						$out = htmlentities($_COOKIE[$str], ENT_HTML5);
					break;
	}
	return $out;
}