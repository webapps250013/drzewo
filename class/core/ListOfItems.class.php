<?php
namespace core;

class ListOfItems {	
	private $cfg;
	private $items = [];
	private $cntr_type = LTYPE_LIST_GENERIC;
	
	private $cntr_tag = "ul";
	private $item_tag = "li";
	
	private $cntr_attr = [];
	private $item_attr = [];
	private $color_prefix = UX_COLOR_MAIN;
	
	// flags
	private $no_link = false;
	private $force_no_link = false;
	private $items_only = false;
	private $new_one = false;
	
	private $max_lines = 0;
	
	public function __construct($cfg, $items = [], $cntr_type = LTYPE_LIST_GENERIC, $cntr_attr = [] ) {
		$this->cfg = $cfg;
		$this->cntr_type = $cntr_type;
			
		$this->cntr_attr = $cntr_attr;
		
		if(isset($cntr_attr["no_link"] ) )
			$this->no_link = true;
	
		foreach($items AS $item) {
			$this->addItem($item);
		}
	}
	
	public function drawSelect() {
		$this->cntr_tag = "select";
		$this->item_tag = "option";
		$sel = (isset($params["selected"] ) ? $params["selected"] : "0");

		return $this->draw();
	}
	
	public function draw($className = "", $cntr_type = LTYPE_LIST_GENERIC) {
		$this->cntr_type = $cntr_type;
		$this->cntr_attr["class"] = $className;
		
		$this->prepare();
		
		if(isset($this->cntr_attr["class"] ) )
			$this->cntr_attr["class"] .= " ".$this->color_prefix."Color";
		else if(strlen($this->color_prefix) > 0)
			$this->cntr_attr["class"] = $this->color_prefix."Color";
		
		if($this->no_link || $this->force_no_link) {
			$lnkIn = '';
			$lnkOut = '';
		}
		else {
			$lnkIn = 'a';
			$lnkOut = '</a>';	
		}
		
		$output = "";
		
		if(!isset($params["itemsOnly"] ) ) {
			$output .= "<".$this->cntr_tag;
			foreach($this->cntr_attr AS $attr => $val) {
				$output .= " ".$attr."='".$val."'";
			}
			$output .= ">";
		}
		
		foreach($this->items AS $item) {
			$output .= "<".$this->item_tag;
			foreach($this->item_attr AS $attr => $val)
				$output .= " ".$attr."='".$val."'";
			if(isset($item["itemTagAttrs"] ) )			// [? tmp]
				foreach($item["itemTagAttrs"] AS $attr => $val)
					$output .= " ".$attr."='".$val."'";
			$output .= ">";
			if($lnkIn != '') {
				$output .= "<".$lnkIn;
				foreach($item["itemLnkAttr"] AS $lnkAttr => $lnkVal ) {
					$output .= " ".$lnkAttr."='".$lnkVal."'";
				}
				$output .= ">";
			}
			$output .= $item["itemTagContent"].$lnkOut."</".$this->item_tag.">";
		}
		
		if(!isset($params["itemsOnly"] ) )
			$output .= "</".$this->cntr_tag.">";
		
		return $output;
	}
	public function getFirst() {
		return reset($this->items);
	}
	public function getLast() {
		return end($this->items);
	}
	public function getItem($key) {
		if(isset($this->items[$key] ) )
			return $this->items[$key];
		else
			return null;
	}
	public function getItems() {
		return $this->items;
	}
	public function getAmount() {
		return count($this->items);
	}
	public function addItem($obj, $extra = [] ) {
		$this->color_prefix = $obj->getColorPrefix();
		$this->items[]["obj"] = $obj;
		end($this->items);
		$no = key($this->items);
		foreach($extra as $ex_item) {
			$this->items[$no]["extra"][] = $ex_item;
		}
		$this->new_one = true;
	}
	public function rmItem($objId) {
		foreach($this->items as $k=>$itm) {
			if($itm["obj"]->getId() == $objId)
				unset($this->items[$k] );
		}
	}
	public function merge($sList) {
		$sItems = $sList->getItems();
		foreach($sItems as $sItm) {
			$match = 0;
			foreach($this->getItems() as $itm) {
				if($sItm["obj"]->getId() === $itm["obj"]->getId() ) {
					$match = 1;
					break;
				}
			}
			if(!$match)
				$this->addItem($sItm["obj"] );
		}
	}
	
	
	private function prepare() {
		
		switch($this->cntr_type) {
			case LTYPE_SELECT_GENERIC:
			case 'SELECT':	
					$this->cntr_tag = "select";
					$this->item_tag = "option";
					$this->force_no_link = true;

					$sel = (isset($params["selected"] ) ? $params["selected"] : "0");
					
					foreach($this->items AS $no => $itm) {
						if($sel == $itm["obj"]->getId() )
							$items[$no]["itemTagAttrs"]["selected"] = "selected";
						$this->items[$no]["itemTagAttrs"]["value"] = $itm["obj"]->getId();
						$this->items[$no]["itemTagContent"] = $itm["obj"]->getName();
					}
					break;
			case LTYPE_LIST_TILE:
			case "TILE":	
					$this->force_no_link = true;
					foreach($this->items AS $no => $itm) {
						$this->items[$no]["itemTagContent"] = $itm["obj"]->getTile();
					}
					break;
			case LTYPE_LIST_IMG:
					foreach($this->items AS $no => $itm) {
						$this->items[$no]["itemTagAttrs"]["onmouseover"] = "fetchTile(&quot;".$item[$itemId]."&quot;, &quot;".$this->getObjType()."&quot;)";
						$this->items[$no]["itemTagAttrs"]["onmouseout"] = "displayType(floatBox)";
						$items[$no]["itemTagContent"] = $itm["obj"]->getName()/*.extraTxt($items[$no], $itemStr)*/;
					}
					break;
			case LTYPE_INLINE:
					$this->cntr_tag = "span";
					$this->item_tag = "span";
					foreach($this->items AS $no => $itm) {
						$this->items[$no]["itemTagContent"] = $itm["obj"]->getName();
						$this->items[$no]["itemTagAttrs"]["class"] = "list-inline";
						$this->items[$no]["itemLnkAttr"]["href"] = "?".$itm["obj"]->getObjType()."=".$itm["obj"]->getId();
					}
					break;
			case LTYPE_INLINE_TILE:
					$this->cntr_tag = "div";
					$this->item_tag = "span";
					$this->force_no_link = true;
					foreach($this->items AS $no => $itm) {
						$this->items[$no]["itemTagContent"] = $itm["obj"]->getTile();
					}
					break;
			case LTYPE_LIST_GENERIC:
			default:
					$this->cntr_tag = "ul";
					$this->item_tag = "li";
					foreach($this->items AS $no => $itm) {
						$this->items[$no]["itemTagAttrs"]["onmouseover"] = "fetchTile(&quot;".$itm["obj"]->getId()."&quot;, &quot;".$itm["obj"]->getObjType()."&quot;)";
						$this->items[$no]["itemTagAttrs"]["onmouseout"] = "displayType(floatBox)";
						$this->items[$no]["itemTagContent"] = $itm["obj"]->getName();
						$this->items[$no]["itemLnkAttr"]["href"] = "?".$itm["obj"]->getObjType()."=".$itm["obj"]->getId();
					}
		}
		$this->new_one = false;
	}
}
