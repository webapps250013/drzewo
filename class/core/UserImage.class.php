<?php
namespace core;

final class UserImage extends UserFile {
	private $obj;			// obiekt dziedziczący po DbObj
	private $str;			// tytuł obrazu/ikona
	private $alt;			// tekst alternatywny dla obrazu
	private $cls;			// klasy znacznika img
	private $eid;			// id znacznika img
	private $icn = false;	// ikona (encja HTML) zamiast obrazu
	
	public function __construct($cfg, $obj, $str = null, $ext = "png", $alt = null) {
		$this->cfg = $cfg;
		$this->obj = $obj;
		$this->str = ($str === null ? $obj->getName() : $str);
		$this->alt = ($alt === null ? "Obraz" : $alt);
		$this->fnm = $obj->getId();
		$this->ext = $ext;
		
		switch($this->obj->getObjType() ) {
			case OBJ_PERSON:
					$this->src = GLOB_DIR_IMH."/".DB_NAME."/mini";
					$this->dft = $this->src."/".$this->obj->sex.($this->obj->lifetime->hasEnd() ? "Zm" : "").".png";
					break;
			case OBJ_PLACE:
					$this->src = GLOB_DIR_IMP."/".DB_NAME;
					$this->dft = $this->src."/"."0.png";
					break;
			case OBJ_IMAGE:
					$this->src = GLOB_DIR_IMI."/".DB_NAME;
					$this->dft = $this->src."/"."0.png";
					break;
			case OBJ_EVENT:
					$this->icn = true;
					break;
			case OBJ_SHIP:
					$this->icn = true;
					break;
		}
	}
	public function draw($img_type = "TILE") {
		$cls = "";
		$id = "";
		
		switch($img_type) {
			case "TILE":
					$cls = "";
					break;
			case "TILE-SIDE":
					$cls = "";
					break;
			case "BOX":
					$cls = "";
					break;
			case "BOX-GALLERY":
					$this->src .= "/v500px";
					$cls = "";
					break;
			case "GALLERY":
					$cls = "";
					break;
		}
		
		if($this->icn) {
			return "<span class='foto64 fontLarge'>".$this->str."</span>";
		}
		else {
			return "<img src='".$this->fileFix()."'".($cls != "" ? " class='".$cls."'" : "")
						." alt='".$this->alt."'"
						." title='".$this->str."'"
					."/>";
		}
	}
}
