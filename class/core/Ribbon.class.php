<?php
namespace core;

class Ribbon {
	private $items = [];
	
	public function __construct($cfg, $items = [] ) {	//items_left, items_right
		$this->items = $items;
		foreach($cfg->def_rbb AS $item) {
			$this->items[] = $item;
		}
	}
	public function draw() {
		$out =	'<header class="mainColor">'
					.'<ul class="headerMenu">';
		foreach($this->items AS $item) {
			$out .= 	'<li onmouseover="showHeaderList(this)" onmouseout="hideHeaderList(this)">'
							.'<a href="index.php?'.constFix($item[2] )."=".$item[3].'">'
								.'<span class="displayIcon">'.$item[1].'</span>'
								.'<span class="displayText">'.constFix($item[0] ).'</span>'
							.'</a>'
						.'</li>';
		}
		return $out."</ul></header>";
	}
	public function addItemLeft($array) {
		$this->items[] = $array;
	}
}
