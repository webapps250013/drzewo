<?php
namespace core;

class DocHead {
	private $styles = [];
	private $title;
	
	public function __construct($cfg, $title) {
		$this->title = $title;
		//UX_APPNAME
		foreach($cfg->def_stl AS $style) {		// [?]
			$this->addStyle($style);
		}
	}
	public function draw() {
		$out = "<head>"
					."<title>".$this->title."</title>"
					.'<meta charset="UTF-8" />'
					.'<link rel="icon" type="image/png" href="'.GLOB_DIR_UX.'/fav.png" />';
		foreach($this->styles AS $style) {
			$out .= '<link href="'.GLOB_DIR_CSS."/".$style[0].'" type="text/css" rel="stylesheet" media="'.$style[1].'"/>';
		}
		$out .= "</head>";
		return $out;
	}
	public function addStyle($str) {
		$this->styles[] = $str;
	}
}
