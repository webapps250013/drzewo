<?php
namespace core;

class Footer {
	private $items = [];
	
	public function __construct($cfg, $items = [] ) {	//items_big, items_small
		$this->items = $items;
		foreach($cfg->def_ftr AS $item) {
			$this->items[] = $item;
		}
	}
	public function draw() {
		$out = '<footer class="mainColorT">';
		$out .= "<ul>";
		foreach($this->items AS $item) {
			$out .= "<li>".constFix($item[0] )."</li>";
		}
		return $out."</ul></footer>";
	}
}
