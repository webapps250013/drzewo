<?php
namespace core;

class UserLang {
	public $code;
	public $ordinal;
	
	public function __construct($cfg, $code = LANG_DEFAULT) {
		$code = inputFix("GET", "lang", inputFix("COOKIE", "drzewoLANG", LANG_DEFAULT) );
		$this->code = $code;
		$this->ordinal = numfmt_create($code, \NumberFormatter::ORDINAL);

		foreach($cfg->modules AS $mName) {
			$lngFile = new UserFile($cfg, GLOB_DIR_LNG, $mName."_".$this->code, "php");
			if($lngFile->setDefault(GLOB_DIR_LNG, $mName."_".LANG_DEFAULT, "php") ) {
				include_once($lngFile->getFileString() );
			}
		}
	}
	
	public function setLangCookie() {
		setcookie("drzewoLANG", $this->code, time() + (86400 * 30), "/");
	}
}
