<?php
/*
PARAMETRY
  -- getQuery2 --
    val     - wartości dla obiektów bazy danych, oddzielone "-", WYMAGANY
    type    - 012, OPCJONALNY
          0 - rodzaj zwracanych danych: kafel, lista, etc., DOMYŚLNE: ?
          1 - obiekt bazy danych, DOMYŚLNE: ?
          2 - parametr do obiektu, DOMYŚLNE: ?
    param   - 012, przełączniki wyświetlania; OPCJONALNY, 
          0 - lType, rodzaj listy, DOMYŚLNE: LTYPE_LIST_GENERIC
          1 - TBA
          2 - TBA
    lang    - oznaczenie języka, OPCJONALNY, DOMYŚLNE: oznaczenie z ciasteczka
*/
  include("class/core/Config.class.php");
  $cfg = new \core\Config("user");
  
  $db = $cfg->db;

  $type = \core\inputFix("GET", "type", "");
  while(strlen($type) < 3)
    $type .= "0";
  $val = \core\inputFix("GET", "val", "");
  if(strlen($val) == 0) exit(ERR_A00_T);
    $param = \core\inputFix("GET", "param", LTYPE_LIST_GENERIC);
  while(strlen($param) < 3)
    $param .= "0";
  //$cfg->debug1 .= "TYPE:".$type." | VAL:".$val." | PARAM:".$param." | LANG:".$lang."<br/>";

  switch($type[0] ) {
  /////////////////////////////////////////////////// tile return
    case FETCH_TYPE_TILE:
          switch($type[1] ) {
            case OBJ_PERSON: echo (new \tile\TilePerson($cfg, $val, "i") )->draw(); break;  // kafel-osoba
            case OBJ_PLACE:  echo (new \tile\TilePlace($cfg, $val, "i") )->draw();  break;  // kafel-miejsce
            case OBJ_EVENT:  echo (new \tile\TileEvent($cfg, $val, "i") )->draw();  break;  // kafel-wydarzenie
            //case OBJ_SRC:    echo tileSrc($val);                              break;  // kafel-źródło
            default:         echo (new \tile\TileErr($cfg, $type) )->draw();        break;  // kafel-błąd
          }
          break;
  /////////////////////////////////////////////////// box return
    case FETCH_TYPE_BOX:
          switch($type[1] ) {
            case OBJ_PERSON: echo (new \box\BoxPerson($cfg, $val) )->draw(); break;  // okno-osoba
            case OBJ_PLACE:  echo (new \box\BoxPlace($cfg, $val) )->draw();  break;  // okno-miejsce
            case OBJ_EVENT:  echo (new \box\BoxEvent($cfg, $val) )->draw();  break;  // okno-wydarzenie
            //case OBJ_SRC:    echo BoxSrc($val);                         break;  // okno-źródło
            //default:         echo (new \box\BoxErr($cfg, $type) )->draw();   break;  // okno-błąd
          }
          break;
  /////////////////////////////////////////////////// list return
    case FETCH_TYPE_LIST:
          switch($type[1] ) {
            case OBJ_PERSON:
                  switch($type[2] ) {              // lista-osoba
                    case FETCH_PERSON_PARENTS:     //  lista-rodzice
                          // val -> ID dziecka
                          $prnts = $db->getDbData("getParents", [$val], OBJ_PERSON);
                          if($prnts->getAmount() > 0) {
                            echo $prnts->draw("", $param[0] ); //["itemsOnly" => true]
                          }
                          else exit(\core\constFix("ERR_A02_T") );
                          break;
                    case FETCH_PERSON_FATHER:      //  lista-ojciec
                          // val -> ID dziecka
                          $prnts = $db->getDbData("getPersonData", ["getFather(".$val.") )"], OBJ_PERSON);
                          if($prnts->getAmount() > 0) {
                            $fId = $db->getDbDataArray("getFather", [$val], "0");
                            $prnts[0][] = $fId[0][0];
                            echo $prnts->draw("", $param[0] ); //["itemsOnly" => true]
                          }
                          else exit(\core\constFix("ERR_A02_T") );
                          break;
                    case FETCH_PERSON_CHILDREN:    //  lista-dzieci
                          // val -> ojciec | matka
                          $val = explode("-", $val);
                          if(count($val) < 2) $val[1] = "NULL";
                          $chd = $db->getDbData("getChildren", [$val[0], $val[1] ], OBJ_PERSON);
                          if($chd->getAmount() > 0)
                            echo $chd->draw("", $param[0] ); //["itemsOnly" => true]
                          else exit(\core\constFix("ERR_A03_T") );
                          break;
                    case FETCH_PERSON_SIBLINGS:    //  lista-rodzeństwo
                          // val -> ojciec | matka | probant
                          $val = explode("-", $val);
                          if(count($val) < 3) $val[1] = "NULL";
                          if(count($val) < 3) $val[2] = "NULL";
                          $chd1 = $db->getDbData("getChildren", [$val[0], 0], OBJ_PERSON);
                          $chd2 = $db->getDbData("getChildren", [$val[1], 0], OBJ_PERSON);
                          $chd1->merge($chd2);
                          $chd1->rmItem($val[2] );
                          
                          if($chd1->getAmount() > 0)
                            echo $chd1->draw("", $param[0] ); //["itemsOnly" => true]
                          else exit(\core\constFix("ERR_A01_T") );
                          break;
                    case FETCH_PERSON_SHIPS:       //  lista-związki
                          // val -> hid1 | hid2
                          $val = explode("-", $val);
                          if(count($val) < 2) $val[1] = "NULL";
                          $chd = $db->getDbData("getShipData", ["NULL", $val[0], $val[1] ], OBJ_SHIP);
                          $pIndex = 1;
                          $pStr = "3";
                          if($chd->getAmount() > 0) {
                            echo $chd->draw("", $param[0] ); //["itemsOnly" => true]
                          }
                          else exit(\core\constFix("ERR_A03_T") );
                          break;
                    case FETCH_PERSON_EVENTS:      //  lista-wydarzenia
                          $evts = $db->getDbData("getPersonEvents", [$val], OBJ_EVENT);
                          if($evts->getAmount() > 0)
                            echo $evts->draw("", $param[0] ); //["itemsOnly" => true]
                          else exit(\core\constFix("ERR_A00_T") );
                          break;
                    case FETCH_SEARCH_RESULT:      //  wyświetlanie wyników szukajki
                          $sRes = $db->getDbData("getSearchResult", [$val], OBJ_PERSON);
                          if($sRes->getAmount() > 0)
                            echo $sRes->draw("", $param[0] ); //["itemsOnly" => true]
                          else exit(\core\constFix("ERR_A00_T") );
                          break;
                    default:
                          echo "Nie można przygotować listy dla podanych parametrów.";
                  }
                  break;
            case OBJ_PLACE:
                  switch($type[2] ) {              // lista-miejsce
                    case FETCH_PLACE_EVENTS:       //  lista-wydarzenia
                          $wyd = $db->getDbDataArray('getPlaceEvents', [$val] );
                          echo "err(".count($wyd)."):".mysqli_errno($baza).". ".mysqli_error($baza);
                          /*foreach($wyd AS $k => $v)
                            $wyd[$k][] = dateFix($wyd[$k][3], UX_DATE_FLAG_NOW, 2).", ".$wyd[$k][2];*/
                          if(count($wyd) > 0)
                            echo (new \core\listOfItems($cfg, $wyd,'LIST-IMG', OBJ_EVENT, [], ["1", "8"], 0, [], ["itemsOnly" => true] ) )->draw();
                          else exit(\core\constFix("ERR_A00_T") );
                          break;
                    default:
                          echo "Nie można przygotować listy dla podanych parametrów.";
                  }
                break;
          }
          break;
  /////////////////////////////////////////////////// wyświetlanie symboli
    case FETCH_TYPE_SYM:
           switch($type[1] ) {
             case OBJ_SHIP:
                   $val = explode("-", $val);
                   if(count($val) < 2) exit(\core\constFix("ERR_A04_T") );
                   $zw = $db->getDbDataArray("getShipData", [0, (int) $val[0], (int) $val[1] ] );
                   if(count($zw) > 0) {
                     echo "<span title='".$zw[0][12]."'>".$zw[0][15]."</span>";
                   }
                   break;
          }
          break;
  }
?>