<?php
// module: TIMELINE
///////////////////////////////////////// REQ
  include_once("core.php");

///////////////////////////////////////// GENERAL | OGÓLNE
  define("ATTR_TLINE",        "v");
  define("MODULE_TLINE",      "v");
  define("MODULE_TLINE_NAME", "timeline");
  
  define("TIMELINE_COLOR_LIVE", "Live");
  define("TIMELINE_COLOR_DEAD", "Dead");
  define("TIMELINE_COLOR_BCA",  "CaBh");
  define("TIMELINE_COLOR_DCA",  "CaDh");
  define("TIMELINE_COLOR_LCA",  "CaLf");

///////////////////////////////////////// STYLE
  $this->addStyleDef( ["timeline.css", "screen"] );

///////////////////////////////////////// CNT
  $this->addCntDef( [ATTR_TLINE, GLOB_DIR_CNT."/timeline.php", "GET"] );
