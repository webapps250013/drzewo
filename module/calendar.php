<?php
// module: CALENDAR
///////////////////////////////////////// REQ
  include_once("core.php");
  
///////////////////////////////////////// GENERAL | OGÓLNE
  define("MODULE_CALENDAR",      "c");
  define("MODULE_CALENDAR_NAME", "calendar");

///////////////////////////////////////// CNT
  $this->addCntDef( [MODULE_CALENDAR, GLOB_DIR_CNT."/calendar.php", "GET"] );

///////////////////////////////////////// HEADER ITEMS
  $this->addRibbonItemDef( ["UX_LABEL_CALENDAR", "&#128506;", "MODULE_CALENDAR", "0", "", ""] );
?>