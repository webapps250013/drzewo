<?php
// module: GALLERY
///////////////////////////////////////// REQ
  include_once("box.php");
  include_once("graph_js.php");

///////////////////////////////////////// GENERAL | OGÓLNE
  define("MODULE_GALLERY",      "i");
  define("MODULE_GALLERY_NAME", "gallery");

///////////////////////////////////////// FILES | PLIKI
  include_once(GLOB_DIR_CLS."/gallery/BoxGallery.class.php");

///////////////////////////////////////// DIR | FOLDERY
  define("GLOB_DIR_IMI", GLOB_DIR_IMG."/gallery");    // images related to images table

///////////////////////////////////////// CNT
  $this->addCntDef( [OBJ_IMAGE."f", GLOB_DIR_CNT."/gallery.php", "GET"] );

///////////////////////////////////////// BOX
  $this->addBoxDef("MODULE_GALLERY", "MODULE_GALLERY", "GET", "\gallery\BoxGallery", [] );
