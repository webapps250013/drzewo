<?php
// ładowanie modułów
// 2022-10-27
	if(!isset($this->modules) )
		exit("Err: no mod");

	$mDir = dir("module/");
	while($entryName = $mDir->read() )
		if( (strlen($entryName) > 5 
				&& substr($entryName, -3) == "php") ) {
			$mName = explode(".", $entryName);
			$this->modules[] = $mName[0];
			include_once($entryName);
		}
	$mDir->close();