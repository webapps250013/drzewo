<?php
// module: BOX
///////////////////////////////////////// REQ
  include_once("core.php");

///////////////////////////////////////// GENERAL | OGÓLNE
  define("MODULE_BOX",      "b");
  define("MODULE_BOX_NAME", "box");

///////////////////////////////////////// FETCH
  define("FETCH_TYPE_BOX",  "B");

///////////////////////////////////////// CLASS | KLASY
  include_once(GLOB_DIR_CLS."/box/Box.class.php");
  
  include_once(GLOB_DIR_CLS."/box/BoxHeader.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxContent.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxButtons.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxLinks.class.php");
  
  include_once(GLOB_DIR_CLS."/box/BoxEvent.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxPerson.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxPlace.class.php");
  include_once(GLOB_DIR_CLS."/box/BoxSearch.class.php");

///////////////////////////////////////// STYLE
  $this->addStyleDef( ["box.css", "screen"] );

///////////////////////////////////////// RIBBON ITEMS | ELEMENTY WSTĄŻKI
  $this->addRibbonItemDef( ["UX_LABEL_PLACES", "&#128506;", "OBJ_PLACE", "0", "", ""] );
  $this->addRibbonItemDef( ["UX_LABEL_EVENTS", "&#128198;", "OBJ_EVENT", "0#wydDzis", "", ""] );

///////////////////////////////////////// BOXES | OKNA INFORMACJI
  $this->addBoxDef("MODULE_BOX", "OBJ_PERSON",  "GET",  "\box\BoxPerson", [] );
  $this->addBoxDef("MODULE_BOX", "OBJ_PLACE",   "GET",  "\box\boxPlace",  [] );
  $this->addBoxDef("MODULE_BOX", "ATTR_SEARCH", "POST", "\box\boxSearch", [] );
  $this->addBoxDef("MODULE_BOX", "OBJ_EVENT",   "GET",  "\box\boxEvent",  [] );
