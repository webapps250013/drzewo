<?php
///////////////////////////////////////// GENERAL | OGÓLNE
  define("APP_NAME", "Drzewo");
  define("APP_VERS", "4");
  define("APP_BULD", "230329");
  
  define("MODULE_CORE", "");
  define("MODULE_CORE_NAME", "core");

///////////////////////////////////////// ATTR | ATRYBUTY URI
  define("ATTR_ABOUT",  "1");
  define("ATTR_MAIN",   "0");
  define("ATTR_SEARCH", "q");
  define("ATTR_STATS",  "k");

///////////////////////////////////////// DATABASE | BAZA DANYCH
  define("OBJ_GENERIC", "0");
  define("OBJ_EDIT",    "j");
  
  define("OBJ_PERSON",  "h");
  define("OBJ_PLACE",   "p");
  define("OBJ_EVENT",   "e");
  define("OBJ_IMAGE",   "i");
  define("OBJ_NPART",   "r");
  define("OBJ_SHIP",    "m");
  define("OBJ_LINK",    "l");
  define("OBJ_WEBSITE", "w");
  define("OBJ_NAME",    "n");
  define("OBJ_DOC",     "d");
  define("OBJ_SRC",     "s");

///////////////////////////////////////// FETCH
  define("FETCH_TYPE_LIST",       "L");
  define("FETCH_TYPE_TILE",       "K");
  define("FETCH_TYPE_SYM",        "Z");
  
  define("FETCH_PERSON_PARENTS",  "R");
  define("FETCH_PERSON_FATHER",   "F");
  define("FETCH_PERSON_CHILDREN", "C");
  define("FETCH_PERSON_SIBLINGS", "S");
  define("FETCH_PERSON_EVENTS",   "E");
  define("FETCH_PERSON_SHIPS",    "M");
  
  define("FETCH_PLACE_EVENTS",    "E");
  
  define("FETCH_SEARCH_RESULT",   "V");

///////////////////////////////////////// DIR | FOLDERY
  define("GLOB_DIR_DEV", "_dev");                  // development
  define("GLOB_DIR_MOD", "module");                // app modules
  define("GLOB_DIR_DB",  "db");                    // db-related
  define("GLOB_DIR_RES", "res");                   // resources
  define("GLOB_DIR_LNG", "lang");                  // languages
  define("GLOB_DIR_CNT", "cnt");                   // main container fillers
  define("GLOB_DIR_FCN", "fcn");                   // functions
  define("GLOB_DIR_CLS", "class");                 // classes
  define("GLOB_DIR_JS",  GLOB_DIR_RES."/js");      // JavaScript files
  define("GLOB_DIR_CSS", GLOB_DIR_RES."/css");     // cascade stylesheets
  define("GLOB_DIR_FNT", GLOB_DIR_RES."/font");    // fonts
  define("GLOB_DIR_IMG", GLOB_DIR_RES."/img");     // db-related images repository
  define("GLOB_DIR_UX",  GLOB_DIR_IMG."/ux");      // ux resources
  define("GLOB_DIR_IMH", GLOB_DIR_IMG."/person");  // people images
  define("GLOB_DIR_IMP", GLOB_DIR_IMG."/place");   // places' images

///////////////////////////////////////// UX | INTERFEJS
  define("GLOB_F", "K");
  define("GLOB_M", "M");
  define("UX_SYM_F", "&#9679;");
  define("UX_SYM_M", "&#9632;");
  define("UX_IMG_F", "&#128105;");
  define("UX_IMG_M", "&#128104;");
  
  define("UX_BTT_EDIT", "&#9998;");
  define("UX_BTT_CLOSE","&#10006;");
  
  define("UX_DATE_FLAG_BEFORE",    "0");
  define("UX_DATE_FLAG_MILLENIUM", "1");
  define("UX_DATE_FLAG_CENTURY",   "2");
  define("UX_DATE_FLAG_CIRCA",     "3");
  define("UX_DATE_FLAG_YEAR",      "4");
  define("UX_DATE_FLAG_MONTH",     "5");
  define("UX_DATE_FLAG_DAY",       "6");
  define("UX_DATE_FLAG_AFTER",     "8");
  define("UX_DATE_FLAG_NOW",       "9");
  
  define("UX_COLOR_MAIN",   "main");
  define("UX_COLOR_PERSON", "person");
  define("UX_COLOR_DEAD",   "dead");
  define("UX_COLOR_EVENT",  "event");
  define("UX_COLOR_IMAGE",  "photo");
  define("UX_COLOR_PLACE",  "place");
  
  define("LTYPE_LIST_GENERIC",   "0");
  define("LTYPE_LIST_IMG",       "1");
  define("LTYPE_LIST_TILE",      "2");
  define("LTYPE_SELECT_GENERIC", "3");
  define("LTYPE_SELECT_TILE",    "4");
  define("LTYPE_TREE",           "5");
  define("LTYPE_INLINE",         "6");
  define("LTYPE_INLINE_TILE",    "7");

///////////////////////////////////////// TILE
  define("TILE_TYPE_PARENT", "PRNT");
  define("TILE_TYPE_CHILD",  "CHLD");
  define("TILE_TYPE_INFO",   "INFO");

///////////////////////////////////////// PERSON NAME FORMAT
  define("INFO_UPPER_IND", "UPPER");
  define("INFO_LOWER_IND", "LOWER");
  define("INFO_CAPED_IND", "CAPED");
  define("INFO_NOTHG_IND", "NOTHG");

///////////////////////////////////////// CLASS | KLASY
  include_once(GLOB_DIR_CLS."/core/core.php");

  include_once(GLOB_DIR_CLS."/core/Config.class.php");
  include_once(GLOB_DIR_CLS."/core/Container.class.php");
  include_once(GLOB_DIR_CLS."/core/DocHead.class.php");
  include_once(GLOB_DIR_CLS."/core/Document.class.php");
  include_once(GLOB_DIR_CLS."/core/Footer.class.php");
  include_once(GLOB_DIR_CLS."/core/Lifetime.class.php");
  include_once(GLOB_DIR_CLS."/core/ListOfItems.class.php");
  include_once(GLOB_DIR_CLS."/core/Note.class.php");
  include_once(GLOB_DIR_CLS."/core/Ribbon.class.php");
  include_once(GLOB_DIR_CLS."/core/UserFile.class.php");
  include_once(GLOB_DIR_CLS."/core/UserImage.class.php");
  include_once(GLOB_DIR_CLS."/core/UserLang.class.php");
  
  include_once(GLOB_DIR_CLS."/db/DbData.class.php");
  include_once(GLOB_DIR_CLS."/db/DbObj.class.php");
  
  include_once(GLOB_DIR_CLS."/db/DbDoc.class.php");
  include_once(GLOB_DIR_CLS."/db/DbEvent.class.php");
  include_once(GLOB_DIR_CLS."/db/DbImage.class.php");
  include_once(GLOB_DIR_CLS."/db/DbLink.class.php");
  include_once(GLOB_DIR_CLS."/db/DbName.class.php");
  include_once(GLOB_DIR_CLS."/db/DbNPart.class.php");
  include_once(GLOB_DIR_CLS."/db/DbPerson.class.php");
  include_once(GLOB_DIR_CLS."/db/DbPlace.class.php");
  include_once(GLOB_DIR_CLS."/db/DbPrefix.class.php");
  include_once(GLOB_DIR_CLS."/db/DbShip.class.php");
  
  include_once(GLOB_DIR_CLS."/tile/Tile.class.php");
  include_once(GLOB_DIR_CLS."/tile/TileErr.class.php");
  include_once(GLOB_DIR_CLS."/tile/TileEvent.class.php");
  include_once(GLOB_DIR_CLS."/tile/TilePerson.class.php");
  include_once(GLOB_DIR_CLS."/tile/TilePlace.class.php");

///////////////////////////////////////// STYLE
  $this->addStyleDef( ["core.css", "screen"] );

///////////////////////////////////////// RIBBON ITEMS | ELEMENTY WSTĄZKI
  $this->addRibbonItemDef( ["UX_LABEL_STATS", "&#128202;", "ATTR_STATS", "1", "", ""] );

///////////////////////////////////////// FOOTER ITEMS | ELEMENTY STOPKI
  $this->addFooterItemDef( ["UX_LABEL_HELP_PAGE", "ATTR_ABOUT", "1", "", ""] );
  $this->addFooterItemDef( ["UX_LABEL_CREDITS",   "ATTR_ABOUT", "1", "", ""] );
  $this->addFooterItemDef( ["UX_LABEL_SRC",       "ATTR_ABOUT", "1", "", ""] );
  $this->addFooterItemDef( ["UX_LABEL_COOKIES",   "ATTR_ABOUT", "1", "", ""] );

///////////////////////////////////////// CNT
  $this->addCntDef( [ATTR_MAIN,  GLOB_DIR_CNT."/main.php",  "GET"] );
  $this->addCntDef( [ATTR_STATS, GLOB_DIR_CNT."/stats.php", "GET"] );
  $this->addCntDef( [ATTR_ABOUT, GLOB_DIR_CNT."/about.php", "GET"] );
