<?php
// module: EXPORT
///////////////////////////////////////// REQ
  include_once("core.php");

///////////////////////////////////////// GENERAL | OGÓLNE
  define("ATTR_EXPORT",        "x");
  define("MODULE_EXPORT",      "x");
  define("MODULE_EXPORT_NAME", "export");

///////////////////////////////////////// DIR | FOLDERY
  define("GLOB_DIR_JSE", "export");             // JS-version generator files

///////////////////////////////////////// CNT
  // $this->addCntDef( [MODULE_EXPORT, GLOB_DIR_JSE."/ex.php", "GET"] ); // [? indev]
