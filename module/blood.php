<?php
// module: BLOOD
///////////////////////////////////////// REQ
  include_once("box.php");

///////////////////////////////////////// GENERAL | OGÓLNE
  define("ATTR_BLOOD",        "b");
  define("MODULE_BLOOD",      "b");
  define("MODULE_BLOOD_NAME", "blood");

  define("ZWIAZKI_SYM_BRAK_DANYCH",   "?");       // [? depr]
  define("ZWIAZKI_SYM_BRAK_PARTNERA", "");        // [? depr]
  define("ZWIAZKI_SYM_CELIBAT",       "");        // [? depr]
  define("ZWIAZKI_SYM_MALZENSTWO",    "&#9901;"); // [? depr]
  define("ZWIAZKI_SYM_TYLKO_DZIECKO", "&#9892;"); // [? depr]
  define("ZWIAZKI_SYM_NARZECZENSTWO", "&#9903;"); // [? depr]
  define("ZWIAZKI_SYM_KONKUBINAT",    "&#9892;"); // [? depr]

///////////////////////////////////////// CLASS | KLASY
  include_once(GLOB_DIR_CLS."/blood/Blood.class.php");
  include_once(GLOB_DIR_CLS."/blood/BoxBlood.class.php");

///////////////////////////////////////// BOXES | OKNA INFORMACJI
  $this->addBoxDef("MODULE_BLOOD", "ATTR_BLOOD", "GET", "\blood\boxBlood", ["2" => "", "3" => "0"] );
