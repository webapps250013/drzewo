<?php
///////////////////////////////////////// DEFAULTS | DOMYŚLNE  
  define("LANG_DEFAULT", "pl-PL");
  define("PLACE_DEFAULT","0");

///////////////////////////////////////// DATABASE | BAZA DANYCH
  define("DB_HOST", "");
  define("DB_NAME", "");
  define("DB_LOGN", "");
  define("DB_PASS", "");
  define("DB_ADMN", "");
  define("DB_APSS", "");
  
///////////////////////////////////////// UX | INTERFEJS
  define("GLOB_DEBUG_MSG","0");
  
  define("GLOB_ADULT_AGE", "18");
  define("GLOB_CONSENT_AGE", "15");
  
  define("UX_EDIT_ALLOW","1");
  
///////////////////////////////////////// EVENTS | WYDARZENIA
  define("EVENTS_DAYS_FORWARD","5");
  define("EVENTS_DAYS_BACKWARD","5");
  
?>