<?php

/*****************************/
/********* lang file *********/
/*****************************/
/*          pl-PL            */
/*          język polski     */
/*          Polish           */
/*****************************/

///////////////////////////////////////// BOX: BLOOD | OKNO POKREWIEŃSTWA
  define("UX_BOX_BLOOD_BTT_LEFT","Plansza");
  define("UX_BOX_BLOOD_BTT_RIGHT","Sprawdź");
  
  define("UX_BOX_BLOOD_DESC_FROM_BLOOD","Sprawdź, czy te osoby są spokrewnione.");
  define("UX_BOX_BLOOD_DESC_FROM_LAW","Sprawdź, czy te osoby są spokrewnione lub spowinowacone.");
  
  define("UX_BOX_BLOOD_LABEL_BLOOD","Pokrewieństwo");
  define("UX_BOX_BLOOD_LABEL_LAW","Powinowactwo");
  define("UX_BOX_BLOOD_NO_BLOOD","brak pokrewieństwa");
  define("UX_BOX_BLOOD_NO_LAW","brak powinowactwa");
  
  define("BLOOD_PROBAND",						"probant");
  define("BLOOD_PARENT_".GLOB_M,				"ojciec");
  define("BLOOD_PARENT_".GLOB_F,				"matka");
  define("BLOOD_CHILD_".GLOB_M,					"syn");
  define("BLOOD_CHILD_".GLOB_F,					"córka");
  define("BLOOD_SIBLING_".GLOB_M,				"brat");
  define("BLOOD_SIBLING_".GLOB_F,				"siostra");
  define("BLOOD_GRANDPARENT_".GLOB_M,			"dziadek");
  define("BLOOD_GRANDPARENT_".GLOB_F,			"babka");
  define("BLOOD_GRANDCHILD_".GLOB_M,			"wnuk");
  define("BLOOD_GRANDCHILD_".GLOB_F,			"wnuczka");
  define("BLOOD_PARENT_SIBLING_".GLOB_M,		"wuj");
  define("BLOOD_PARENT_SIBLING_".GLOB_F,		"ciotka");
  define("BLOOD_COUSIN_".GLOB_M,				"kuzyn");
  define("BLOOD_COUSIN_".GLOB_F,				"kuzynka");
  define("BLOOD_RELATIVE_".GLOB_M,				"krewny");
  define("BLOOD_RELATIVE_".GLOB_F,				"krewna");
  define("BLOOD_PRA",							"pra");
  
  define("BLOOD_LAW_PARENT_".GLOB_M,			"teść");
  define("BLOOD_LAW_PARENT_".GLOB_F,			"teściowa");
  define("BLOOD_LAW_CHILD_".GLOB_M,				"zięć");
  define("BLOOD_LAW_CHILD_".GLOB_F,				"synowa");
  define("BLOOD_LAW_PARTNER_SIBLING_".GLOB_M,	"szwagier");
  define("BLOOD_LAW_PARTNER_SIBLING_".GLOB_F,	"szwagierka");
  define("BLOOD_LAW_SIBLING_PARTNER_".GLOB_M,	"szwagier");
  define("BLOOD_LAW_SIBLING_PARTNER_".GLOB_F,	"bratowa");
  define("BLOOD_LAW_STEP_PATRENT_".GLOB_M,		"ojczym");
  define("BLOOD_LAW_STEP_PATRENT_".GLOB_F,		"macocha");
  define("BLOOD_LAW_STEP_CHILD_".GLOB_M,		"pasierb");
  define("BLOOD_LAW_STEP_CHILD_".GLOB_F,		"pasierbica");
  define("BLOOD_LAW_STEP_SIBLING_".GLOB_M,		"brat przyrodni");
  define("BLOOD_LAW_STEP_SIBLING_".GLOB_F,		"siostra przyrodnia");
  define("BLOOD_LAW_CHLD_PNTR_PARENT_".GLOB_M,	"swat");
  define("BLOOD_LAW_CHLD_PNTR_PARENT_".GLOB_F,	"swatowa");
  define("BLOOD_LAW_ADOPTION_PARENT_".GLOB_M,	"ojczym");
  define("BLOOD_LAW_ADOPTION_PARENT_".GLOB_F,	"macocha");
  define("BLOOD_LAW_ADOPTION_CHILD_".GLOB_M,	"przysposobiony");
  define("BLOOD_LAW_ADOPTION_CHILD_".GLOB_F,	"przysposobiona");
  
  //define("BLOOD_i_j_P1_sex","pokrewieństwo");

///////////////////////////////////////// ERR | BŁĘDY
?>