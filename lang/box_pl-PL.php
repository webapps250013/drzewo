<?php

/*****************************/
/********* lang file *********/
/*****************************/
/*          pl-PL            */
/*          język polski     */
/*          Polish           */
/*****************************/

///////////////////////////////////////// BOX | OKNO
  define("UX_LIST_LABEL_PEOPLE","Osoby");
  define("UX_LIST_LABEL_PLACES","Miejsca");
  define("UX_LIST_LABEL_EVENTS","Wydarzenia");
  define("UX_LIST_LABEL_NAMES","Personalia");
  define("UX_LIST_LABEL_PARENTS","Rodzice");
  define("UX_LIST_LABEL_SIBLINGS","Rodzeństwo");
  define("UX_LIST_LABEL_CHILDREN","Dzieci");
  define("UX_LIST_LABEL_SAMEFNAME","Osoby o imieniu");
  define("UX_LIST_LABEL_SAMELNAME","Osoby o nazwisku");
  define("UX_LIST_LABEL_SAMEBYEAR","Urodzeni w");
  define("UX_LIST_LABEL_SETTLEMENTS","Miejscowości");
  define("UX_LIST_LABEL_GRAPH","Graf");
  define("UX_LIST_LABEL_RELATED_EVENTS","Powiązane wydarzenia");
  define("UX_LIST_LABEL_SHIPS","Związki");
  define("UX_LIST_LABEL_DESC","Profil");
  
///////////////////////////////////////// BOX: PERSON | OKNO OSOBY
  define("UX_BOX_PERSON_BTT_LEFT","Graf");
  define("UX_BOX_PERSON_BTT_RIGHT","Pokrewieństwo");
  
  define("INFO_Z_DOMU","z domu");

///////////////////////////////////////// BOX: PLACE | OKNO MIEJSCA
  define("UX_BOX_PLACE_BTT_LEFT","Mapa");
  define("UX_BOX_PLACE_BTT_RIGHT","Współrzędne");
  
  define("INFOM_MAPA","Widok mapy");
  define("INFOM_LISTA","Widok listy");
  define("INFOM_M_GOOGLE","Mapy Google");
  
  define("MIEJSCE_0_N","Ziemia");
  define("MIEJSCE_0_T","planeta");
  
///////////////////////////////////////// BOX: EVENT | OKNO WYDARZENIA
  define("UX_BOX_EVENT_BTT_LEFT","Tego dnia");
  define("UX_BOX_EVENT_BTT_RIGHT","Tego roku");

///////////////////////////////////////// BOX: EVENTS | OKNO WYDARZEŃ
  define("UX_BOX_EVENTS_BTT_LEFT","Kalendarz");
  define("UX_BOX_EVENTS_BTT_RIGHT","Rocznice");
  define("UX_BOX_EVENTS_LABEL_LAST","Ostatnio");
  define("UX_BOX_EVENTS_LABEL_TODAY","Dziś");
  define("UX_BOX_EVENTS_LABEL_SOON","Wkrótce");
  
///////////////////////////////////////// BOX: EDIT | OKNO EDYCJI
  define("UX_BOX_EDIT_BTT_LEFT","Anuluj");
  define("UX_BOX_EDIT_BTT_EDIT","Zmień");
  define("UX_BOX_EDIT_BTT_NEW","Dodaj");
  
  define("UX_BOX_EDIT_BTT_ADD_EVENT","Dodaj wydarzenie");
  define("UX_BOX_EDIT_BTT_ADD_NAME","Dodaj miano");
  define("UX_BOX_EDIT_BTT_ADD_NPART","Dodaj człon miana");
  define("UX_BOX_EDIT_BTT_ADD_PERSON","Dodaj osobę");
  define("UX_BOX_EDIT_BTT_ADD_PLACE","Dodaj miejsce");
  define("UX_BOX_EDIT_BTT_ADD_CHILD","Dodaj dziecko");
  define("UX_BOX_EDIT_BTT_ADD_SHIP","Dodaj związek");
  
  define("UX_BOX_EDIT_LABEL_NEW_PERSON","Nowa osoba");
  define("UX_BOX_EDIT_LABEL_NEW_EVENT",	"Nowe wydarzenie");
  define("UX_BOX_EDIT_LABEL_NEW_PLACE",	"Nowe miejsce");
  define("UX_BOX_EDIT_LABEL_NEW_DOC",	"Nowy dokument");
  
  define("UX_BOX_EDIT_CHANGE_DESC",	"Opis zmian");
  define("UX_BOX_EDIT_CHANGE_SRC",	"Źródło zmian");
  
///////////////////////////////////////// BOX: SEARCH | OKNO WYSZUKIWANIA  
  define("UX_BOX_SEARCH_BTT_LEFT","BTT_LEFT");
  define("UX_BOX_SEARCH_BTT_RIGHT","BTT_RIGHT");
  define("SZUKAJKA_WYNIKI","Trafień");

///////////////////////////////////////// ERR | BŁĘDY
?>