<?php

/*****************************/
/********* lang file *********/
/*****************************/
/*          core_pl-PL       */
/*          język polski     */
/*          Polish           */
/*****************************/

///////////////////////////////////////// DATABASE | BAZA DANYCH

///////////////////////////////////////// FAMILY GRAPH | GRAF RODZINY
  define("FGRAPH_NO_CHILDREN",		"rodzina bezdzietna");
  define("FGRAPH_NO_CHILDREN_MORE",	"lub brak informacji");

///////////////////////////////////////// GLOBAL
  define("UX_LANGNAME","polski");
  define("UX_LANGFLAG","&#x1F1F5;&#x1F1F1;");
  define("UX_APPNAME","Drzewo 3.1");
  define("UX_APPNAME_FULL","Drzewo genealogiczne 3.1");
  
  define("GLOB_BRAK_DANYCH","brak danych");
  define("GLOB_ANULUJ","Anuluj");
  define("GLOB_BLAD","Bład");
  define("GLOB_OK","OK");
  define("GLOB_START_BOX","Kogo szukasz?");
  define("GLOB_START_BOX_SZUKAJ","Szukaj");
  define("GLOB_START_BOX_LOSUJ","Losuj!");
  define("GLOB_JS_KOMUNIKAT","Ten element wymaga JavaScriptu.");
  
///////////////////////////////////////// LABEL | ETYKIETY
  define("UX_LABEL_COOKIES","Ciasteczka");
  define("UX_LABEL_HELP_PAGE","Pomoc");
  define("UX_LABEL_CREDITS","Autorzy");
  define("UX_LABEL_SRC","Źródła");
  
  define("UX_LABEL_STATS","Statystyki");
  define("UX_LABEL_PLACES","Miejsca");
  define("UX_LABEL_EVENTS","Wydarzenia");
  define("UX_LABEL_SEARCHBOX","Szukajka");
  
  define("UX_LABEL_CHILD","dziecko");
  define("UX_LABEL_SHIP","związek");
  define("UX_LABEL_PERSON","osoba");
  define("UX_LABEL_PLACE","miejsce");
  define("UX_LABEL_IMG","zdjęcie");
  define("UX_LABEL_DOC","dokument");
  define("UX_LABEL_WEBSITE","strona internetowa");
  
  define("UX_LABEL_SEX","płeć");
  define("UX_LABEL_MAN","mężczyzna");
  define("UX_LABEL_WOMAN","kobieta");
  
  define("UX_LABEL_EVENT","wydarzenie");
  define("UX_LABEL_EVENT_TYPE","rodzaj wydarzenia");
  define("UX_LABEL_START_DATE","data rozpoczęcia");
  define("UX_LABEL_END_DATE","data zakończenia");
  
  define("UX_LABEL_DATE_YEAR","rok");
  define("UX_LABEL_DATE_MTH","miesiąc");
  define("UX_LABEL_DATE_DAY","dzień");
  define("UX_LABEL_DATE_FLAG","flaga");

///////////////////////////////////////// DATE | DATA
  define("UX_DATE_FORMAT","0");
  define("UX_DATE_SEPARATOR",".");
  define("UX_DATE_BD","-");
  
  define("UX_DATE_BEFORE_LABEL","przed");
  define("UX_DATE_MILL_LABEL","milenium");
  define("UX_DATE_CENT_LABEL","wiek");
  define("UX_DATE_CIRCA_LABEL","około");
  define("UX_DATE_YEAR_LABEL","rok");
  define("UX_DATE_MTH_LABEL","miesiąc");
  define("UX_DATE_DAY_LABEL","dzień");
  define("UX_DATE_BETWEEN_LABEL","pomiędzy");
  define("UX_DATE_AFTER_LABEL","po");
  define("UX_DATE_NOW_LABEL","teraz");
  
  define("UX_DATE_BCE_PX","");
  define("UX_DATE_BCE_SX","p.n.e.");
  define("UX_DATE_CE_PX","");
  define("UX_DATE_CE_SX","");
  define("UX_DATE_CIRCA_PX","ok.");
  define("UX_DATE_CIRCA_SX","");
  define("UX_DATE_BEFORE_PX","przed");
  define("UX_DATE_BEFORE_SX","");
  define("UX_DATE_AFTER_PX","po");
  define("UX_DATE_AFTER_SX","");
  define("UX_DATE_CENT_PX","");
  define("UX_DATE_CENT_SX","w.");
  define("UX_DATE_MILL_PX","");
  define("UX_DATE_MILL_SX","tysiąclecie");
  
  define("UX_DATE_MTH_STR_1","styczeń");
  define("UX_DATE_MTH_STR_2","luty");
  define("UX_DATE_MTH_STR_3","marzec");
  define("UX_DATE_MTH_STR_4","kwiecień");
  define("UX_DATE_MTH_STR_5","maj");
  define("UX_DATE_MTH_STR_6","czerwiec");
  define("UX_DATE_MTH_STR_7","lipiec");
  define("UX_DATE_MTH_STR_8","sierpień");
  define("UX_DATE_MTH_STR_9","wrzesień");
  define("UX_DATE_MTH_STR_10","październik");
  define("UX_DATE_MTH_STR_11","listopad");
  define("UX_DATE_MTH_STR_12","grudzień");
  
  define("UX_DATE_MTH_SHRT_1","sty");
  define("UX_DATE_MTH_SHRT_2","lut");
  define("UX_DATE_MTH_SHRT_3","mar");
  define("UX_DATE_MTH_SHRT_4","kwi");
  define("UX_DATE_MTH_SHRT_5","maj");
  define("UX_DATE_MTH_SHRT_6","cze");
  define("UX_DATE_MTH_SHRT_7","lip");
  define("UX_DATE_MTH_SHRT_8","sie");
  define("UX_DATE_MTH_SHRT_9","wrz");
  define("UX_DATE_MTH_SHRT_10","paź");
  define("UX_DATE_MTH_SHRT_11","lis");
  define("UX_DATE_MTH_SHRT_12","gru");
  
  define("UX_DATE_DAY_STR_1","poniedziałek");
  define("UX_DATE_DAY_STR_2","wtorek");
  define("UX_DATE_DAY_STR_3","środa");
  define("UX_DATE_DAY_STR_4","czwartek");
  define("UX_DATE_DAY_STR_5","piątek");
  define("UX_DATE_DAY_STR_6","sobota");
  define("UX_DATE_DAY_STR_7","niedziela");
  
  define("UX_DATE_DAY_SHRT_1","pn");
  define("UX_DATE_DAY_SHRT_2","wt");
  define("UX_DATE_DAY_SHRT_3","śr");
  define("UX_DATE_DAY_SHRT_4","cz");
  define("UX_DATE_DAY_SHRT_5","pt");
  define("UX_DATE_DAY_SHRT_6","sb");
  define("UX_DATE_DAY_SHRT_7","nd");

///////////////////////////////////////// IDENTITIES | TOŻSAMOŚCI
  define("TZ_REKONSTR", "rekonstrukcja");
  define("NM_FNAME", "imię");
  define("NM_LNAME", "nazwisko");
  define("NM_ANAME", "wstawka");
  
  define("INFO_IM_IND", INFO_CAPED_IND);
  define("INFO_NZ_IND", INFO_UPPER_IND);
  define("INFO_AS_IND", INFO_CAPED_IND);
  define("INFO_NZ_BRAK","");
  define("INFO_NZ_BRAK_M","");
  define("INFO_NZ_BRAK_K","");

///////////////////////////////////////// SHIPS | ZWIĄZKI
  define("SHIPS_NO_DATA","brak danych");
  define("SHIPS_NO_DATA_".GLOB_M,"brak danych");
  define("SHIPS_NO_DATA_".GLOB_F,"brak danych");
  define("SHIPS_NO_PARTNER","brak partnera");
  define("SHIPS_NO_PARTNER_".GLOB_M,"wolny");
  define("SHIPS_NO_PARTNER_".GLOB_F,"wolna");
  define("SHIPS_NO_PARTNER_EVER","brak partnera");
  define("SHIPS_NO_PARTNER_EVER_".GLOB_M,"kawaler");
  define("SHIPS_NO_PARTNER_EVER_".GLOB_F,"panna");
  define("SHIPS_CELIBACY","celibat");
  define("SHIPS_CELIBACY_".GLOB_M,"celibat");
  define("SHIPS_CELIBACY_".GLOB_F,"celibat");
  define("SHIPS_PARTNER","para");
  define("SHIPS_PARTNER_".GLOB_M,"partner");
  define("SHIPS_PARTNER_".GLOB_F,"partnerka");

///////////////////////////////////////// STATS | STATYSTYKI
  define("STATS_COUNT_FEM","kobiet");
  define("STATS_COUNT_MAS","mężczyzn");
  define("STATS_COUNT_PPL","osób");
  define("STATS_COUNT_FNM","różnych imion");
  define("STATS_COUNT_LNM","różnych nazwisk");

///////////////////////////////////////// ERR | BŁĘDY
  define("UX_BOX_ERR_BTT_LEFT","Strona główna");
  define("UX_BOX_ERR_BTT_RIGHT","Przeładuj stronę");
  
  define("ERR_000_T","Nie zaimplementowano");
  define("ERR_000_O","Nie można wykonać rządanego działania. Funkcja jest w przygotowaniu.");
  define("ERR_001_T","Nie ma takiej osoby");
  define("ERR_001_O","Sprawdź poprawność podanego ID. Jeżeli przyczyną jest błąd bazy danych - przeładuj stronę.");
  define("ERR_002_T","Błąd");
  define("ERR_002_O","Nieznany błąd.");
  
  define("ERR_A00_T","Brak wyników dla zapytania");
  define("ERR_A00_O","Dla podanych kryteriów nie znaleziono żadnych wyników.");
  define("ERR_A01_T","Brak rodzeństwa");
  define("ERR_A01_O","Ta osoba nie posiada rodzeństwa.");
  define("ERR_A02_T","Brak informacji o rodzicach");
  define("ERR_A02_O","Nie znaleziono informacji o rodzicach danej osoby.");
  define("ERR_A03_T","Brak dzieci");
  define("ERR_A03_O","Ta osoba nie posiada dzieci.");
  define("ERR_A04_T","Brak argumentów");
  define("ERR_A04_O","Nie podano wystarczającej liczby argumentu dla zapytania.");
  define("ERR_A05_T","Błąd składni");
  define("ERR_A05_O","Wystąpił błąd składni.");
  
  define("ERR_D00_T","Błąd bazy danych");
  define("ERR_D00_O","Wystąpił błąd związany z komunikacją z bazą danych.");
  
  define("ERR_I00_T","Nie ma takiego zdjęcia");
  define("ERR_I00_O","Nie ma takiego zdjęcia");
  
  define("ERR_F00_T", "Nie ma takiego pliku");
  define("ERR_F00_O", "Nie ma takiego pliku");
  
  define("ERR_T00_T", "Nie można wyświetlić kafla");
  define("ERR_T00_O", "Nie można wyświetlić kafla");
  
  /***************************
	ERR_x00_t
		kategoria błędu x
			R - request  - 
			D - database - błędy w komunikacji z bazą danych
			I - image    - błędy związane z zapisanymi na serwerze obrazmi
			F - file     - błędy związane z plikami na serwerze
			T - tile     - błędy z wyświetlaniem kafli
			
			
		kolejny nr błedu 00-99
		
		typ stałej t
			T - tytuł błędu
			O - opis błędu
  ***************************/
?>