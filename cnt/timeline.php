<?php
	$ppl = $this->cfg->db->getDbData("getPeopleList", ["birth"], OBJ_PERSON);
	$minYear = date("Y");					// rok "zerowy"
	$maxYear = date("Y");					// rok "dzisiejszy"
	$avg = 70;								// średni wiek człowieka
	$scale = 10;
	
	foreach($ppl->getItems() AS $person) {
		$bYear = (int) $person["obj"]->lifetime->getDateStart(2, 0);
		if($minYear > $bYear && $bYear != '0') {
			echo "min:".$minYear."<br />";
			$minYear = $bYear;
		}
		
	}
	foreach($ppl->getItems() AS $person) {
		$lTime = $person["obj"]->lifetime;
		$bYear = (int)$lTime->getDateStart(2, 0);
		
		$color = TIMELINE_COLOR_LIVE;						// live person color
		
		if(!$lTime->isExactDate('1', UX_DATE_FLAG_YEAR) )
				$color = TIMELINE_COLOR_BCA;				// approx. birth date person color
		if($bYear == '0') continue;							// unknown birth date
		
		$dYear = $maxYear;
		if($lTime->hasEnd() ) {
			$dYear = (int) $person["obj"]->lifetime->getDateEnd(2, 0);
			if($color == TIMELINE_COLOR_LIVE) {
				$color = TIMELINE_COLOR_DEAD;				// dead person color
				if(!$lTime->isExactDate('2', UX_DATE_FLAG_YEAR) )
					$color = TIMELINE_COLOR_DCA;			// approx. death date person color
			}
			else {
				$color = TIMELINE_COLOR_LCA;				// avg. life length person color
				$dYear = (int)$bYear + $avg;
			}
			if($dYear == '0')
				$dYear = (int)$bYear + $avg;
		
		}
		
		$lngt = (int) ($dYear - $bYear ) * $scale;
		$left = ((int)$bYear - (int)$minYear) * $scale;
		
		echo "-->".$lTime->getLength()."<br />";
		
		$out .= "<a href='?".OBJ_PERSON."=".$person["obj"]->getId()."'>"
				."<div class='timelineBar timeline".$color."Color' style='margin-left:".$left."px; width:".$lngt."px;'"
						."onmouseover='fetchTile(&quot;".$person["obj"]->getId()."&quot;)' "
						."onmouseout = 'displayType(floatBox)'>"
				."</div>"
			."</a>";
	}