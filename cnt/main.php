<?php
	if(!isset($this->cfg) )
		exit("Err: No Db");

	$shipNo = 0;
	if(isset($_GET[OBJ_SHIP] ) ) {
		$shipNo = (int) htmlentities($_GET[OBJ_SHIP], ENT_HTML5 );
	}
	else if(isset($_GET[OBJ_SHIP.OBJ_PERSON] ) ) {
		$personId = (int) htmlentities($_GET[OBJ_SHIP.OBJ_PERSON], ENT_HTML5 );
		$person = new \db\DbPerson($this->cfg, $personId);
		do {
			$person = $person->father;
		} while($person->father !== null && count($person->getShips() ) > 0 );
		$shipNo = $person->getShips()[0]->getId();
	}
	else if(isset($_COOKIE["drzewoZID"] ) ) {
		$shipNo = (int) htmlentities($_COOKIE["drzewoZID"], ENT_HTML5);
	}
	$ship = new \db\DbShip($this->cfg, $shipNo);

	if( ($shipPeople = $ship->getPeople() )->getAmount() < 2) {
		if($shipNo != 0)
			$this->notes[] = new \core\Note(ERR_001_O, ERR_001_T);
		
		if( count($shipCount = $this->cfg->db->getDbDataArray("getShipCount", [], 0) ) == 0)
			$shipCount[0][0] = 0;

		$out .=
			"<form method='POST' action='index.php' style='margin-top:5em;'>"
				."<span class='bttActive'>".\core\constFix("GLOB_START_BOX_LOSUJ")."</span>"
			."</form>";
	}
	else {
		setcookie("drzewoZID", $ship->getId(), time() + (86400 * 30), "/");
		$out .=
			"<div id=\"graphShip\">"
				."<div id=\"graphParents\">"
					.(new \tile\TilePerson($this->cfg, $shipPeople->getLast()["obj"], "pi", $ship->getId() ) )->draw()
					.(new \tile\TilePerson($this->cfg, $shipPeople->getFirst()["obj"], "pi", $ship->getId() ) )->draw()
				."</div>"
				."<div id=\"graphShipData\" >"
					."<a href=\"?".\core\constFix("OBJ_EVENT")."=".$ship->getId()."\">"
					."</a>"
					."<br />"
					.$ship->lifetime->getLength(2)
				."</div>"
			."</div>"
			."<div id=\"graphChildren\">";
		if( ($children = $ship->getChildren() )->getAmount() == 0 ) {
			$out .= \core\constFix("FGRAPH_NO_CHILDREN")."<br />".\core\constFix("FGRAPH_NO_CHILDREN_MORE");
		}
		else {
			foreach($children->getItems() AS $chld) {
				$out .= "<div class='graphHLine graphChild' id='tilePerson".$chld["obj"]->getId()."'>
						<div class='graphVLine'>&nbsp;</div>"
						.(new \tile\TilePerson($this->cfg, $chld["obj"], "c") )->draw()
					."</div>";
			}
			$out .= "<script>childLineFix(".$children->getLast()["obj"]->getId().");</script>";
		}
		$out .= "</div>";
	}